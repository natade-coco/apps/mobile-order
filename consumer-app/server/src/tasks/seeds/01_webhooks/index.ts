import { Directus, TypeMap } from '@directus/sdk';

import webhooks from './webhooks.json';

const collection = { name: 'webhooks', data: webhooks };

export async function seed(client: Directus<TypeMap>, internalURL: string) {
  console.log('checking if webhook exists...');
  const check = (await client.transport
    .get(collection.name)
    .catch(() => {})) as any;
  const hooks = check.data as any[];
  collection.data.forEach(async (data) => {
    const same = hooks.filter(
      (h) =>
        JSON.stringify(h.actions) === JSON.stringify(data.actions) &&
        JSON.stringify(h.collections) === JSON.stringify(data.collections) &&
        h.method === data.method
    );
    if (same.length === 0) {
      console.log('webhook internal URL is ', internalURL);
      data.url = `${internalURL}/hooks`;
      //@ts-ignore
      delete data.id;
      const result = await client.transport.post(collection.name, data);
      console.log('items created');
    } else {
      console.log('items already exists');
    }
  });
}

export async function backup(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');

  const check = await client.transport.get(collection.name).catch(console.log);
  if (check && check.data?.length > 0) {
    console.log(collection.name + ' exists');
    fs.writeFileSync(
      path + '/' + collection.name + '.json',
      JSON.stringify(check.data, null, 4)
    );
    console.log('items backup');
  }
}
