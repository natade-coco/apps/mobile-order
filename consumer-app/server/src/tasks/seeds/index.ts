import { Directus, TypeMap } from '@directus/sdk';

import * as Webhooks from '../seeds/01_webhooks';

export function seed(client: Directus<TypeMap>, internalURL: string) {
  Webhooks.seed(client, internalURL);
}

export function backup(client: Directus<TypeMap>) {
  Webhooks.backup(client, './src/tasks/seeds/01_webhooks');
}
