import { makeClient, makeInternalURL } from './utils';
import { seed } from '../seeds';
(async function () {
  const [internalURL, client] = await Promise.all([
    makeInternalURL(),
    makeClient(),
  ]);
  seed(client, internalURL);
})();
