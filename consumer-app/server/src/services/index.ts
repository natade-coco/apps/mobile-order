import { Application } from '../declarations';
import messages from './messages/messages.service';
import orders from './orders/orders.service';
import catalogs from './catalogs/catalogs.service';
import settings from './settings/settings.service';
import carts from './carts/carts.service';
import products from './products/products.service';
import hooks from './hooks/hooks.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(messages);
  app.configure(orders);
  app.configure(catalogs);
  app.configure(settings);
  app.configure(carts);
  app.configure(products);
  app.configure(hooks);
}
