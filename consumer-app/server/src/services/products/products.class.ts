import {
  Id,
  NullableId,
  Paginated,
  Params,
  ServiceMethods,
} from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';

interface Data {}

interface ServiceOptions {}

const COLLECTION_PRODUCT = 'products';

export class Products implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data> | any> {
    const result = await Datastore()
      .items(COLLECTION_PRODUCT)
      .readByQuery(params?.query || {});
    const data = result.data?.map((item: any) => {
      item.image = `${Datastore().url}/assets/${item.image}}`;
      item.prices = item.prices.map((item: any) => item.prices_id);
      return item;
    });
    return data;
  }

  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id,
      text: `A new message with ID: ${id}!`,
    };
  }

  async create(data: Data, params?: Params): Promise<Data> {
    if (Array.isArray(data)) {
      return Promise.all(data.map((current) => this.create(current, params)));
    }

    return data;
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
  async setup() {}
}
