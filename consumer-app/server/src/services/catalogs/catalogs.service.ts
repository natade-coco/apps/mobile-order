// Initializes the `catalogs` service on path `/catalogs`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Catalogs } from './catalogs.class';
import hooks from './catalogs.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'catalogs': Catalogs & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/catalogs', new Catalogs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('catalogs');

  service.hooks(hooks);
}
