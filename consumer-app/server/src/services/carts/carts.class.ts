import {
  Id,
  NullableId,
  Paginated,
  Params,
  ServiceMethods,
} from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';

interface Data {}

interface ServiceOptions {}

const COLLECTION_CART = 'carts';

export class Carts implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data> | any> {
    const result = await Datastore()
      .items(COLLECTION_CART)
      .readByQuery(params?.query || {});
    return result;
  }

  async get(id: Id, params?: Params): Promise<Data | Paginated<Data> | any> {
    const result = await Datastore().items(COLLECTION_CART).readOne(id);
    return result;
  }

  async create(
    data: Data,
    params?: Params
  ): Promise<Data | Paginated<Data> | any> {
    const result = await Datastore().items(COLLECTION_CART).createOne(data);
    return result;
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }

  async setup() {}
}
