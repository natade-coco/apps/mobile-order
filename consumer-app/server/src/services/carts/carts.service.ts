// Initializes the `carts` service on path `/carts`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Carts } from './carts.class';
import hooks from './carts.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'carts': Carts & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/carts', new Carts(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('carts');

  service.hooks(hooks);
}
