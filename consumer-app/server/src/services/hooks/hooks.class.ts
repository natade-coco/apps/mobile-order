import {
  Id,
  NullableId,
  Paginated,
  Params,
  ServiceMethods,
} from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data {}

interface ServiceOptions {}

export class Hooks implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }
  async setup() {}

  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    return [];
  }

  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id,
      text: `A new message with ID: ${id}!`,
    };
  }

  async create(data: any, params?: Params): Promise<Data> {
    const [order, setting]: any = await Promise.all([
      this.app.service('orders').get(data.keys[0], {
        query: { fields: ['*', 'cart.*'] },
      }),
      this.app.service('settings').find(),
    ]).catch((err) => console.error(err));
    if (data.payload.state === 'prepared') {
      const did = this.app.get('natadecoco_id');
      this.app
        .service('messages')
        .create(
          {
            recipient: order.cart.userId,
            title: setting.storeName,
            message: `注文番号${order.reference}の商品が完成しました。カウンターでアプリを開いて、注文番号をご提示ください。`,
            did,
          },
          {}
        )
        .catch((err: any) => console.log(err.response.data.message));
    }
    return order;
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
