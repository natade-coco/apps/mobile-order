// Initializes the `hooks` service on path `/hooks`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Hooks } from './hooks.class';
import hooks from './hooks.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'hooks': Hooks & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/hooks', new Hooks(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('hooks');

  service.hooks(hooks);
}
