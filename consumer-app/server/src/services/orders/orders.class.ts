import {
  Id,
  NullableId,
  Paginated,
  Params,
  ServiceMethods,
} from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';

interface Data {}

interface ServiceOptions {}

const COLLECTION_ORDER = 'orders';

export class Orders implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data> | any> {
    console.log({ query: JSON.stringify(params!.query) });
    const result = await Datastore()
      .items(COLLECTION_ORDER)
      .readByQuery(params?.query || {});
    // .catch(console.log);
    if (result) {
      result.data?.forEach((_i: any) => {
        _i.units = _i.units.map((item: any) => {
          item.image = `${this.app?.get('directus_url')}/assets/${
            item.product.image
          }`;
          return item;
        });
      });
      return result.data;
    }
  }

  async get(id: Id, params?: Params): Promise<Data | any> {
    const result = await Datastore()
      .items(COLLECTION_ORDER)
      .readOne(id, params?.query);
    return result;
  }

  async create(data: any, params?: Params): Promise<Data | any> {
    const result = await Datastore().items(COLLECTION_ORDER).createOne(data);
    return result;
  }

  async update(id: number, data: any, params?: any): Promise<any> {
    const result = await Datastore()
      .items(COLLECTION_ORDER)
      .updateOne(id, data);
    return result;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
  async setup() {}
}
