import { Params } from "@feathersjs/feathers";
import {
  AuthenticationBaseStrategy,
  AuthenticationResult,
} from "@feathersjs/authentication";
import { SDK as Hub, ServiceType } from "@natade-coco/hub-sdk";

export class NatadeCOCOStrategy extends AuthenticationBaseStrategy {
  verifyConfiguration() {
    const config = this.configuration;

    ["tokenField"].forEach((prop) => {
      if (typeof config[prop] !== "string") {
        throw new Error(
          `'${this.name}' authentication strategy requires a '${prop}' setting`
        );
      }
    });
  }

  get configuration() {
    const authConfig = this.authentication?.configuration;
    const config = super.configuration || {};

    return {
      service: authConfig.service,
      entity: authConfig.entity,
      entityId: authConfig.entityId,
      errorMessage: "Invalid token",
      tokenField: config.tokenField,
      ...config,
    };
  }

  async verifyJWT(jwt: string) {
    console.log(jwt);
    const id = this.app?.get("natadecoco_id");
    const secret = this.app?.get("natadecoco_secret");
    const client = await Hub.init({
      id: id,
      secret: secret,
      test: this.app?.get("dev_env") === "stg",
    });
    return client.VerifyJWT(jwt, ServiceType.AppHubService);
  }

  async authenticate(data: AuthenticationResult, params: Params) {
    const { tokenField } = this.configuration;
    const token = data[tokenField];
    const vefiryResult =
      this.app?.get("exec_env") === "global"
        ? await this.verifyJWT(token)
        : { issuer: "did:ethr:0xf5b82fc83b55bfea06577ce1392fcc9e1e8e6468" };
    let cart;
    const result = await this.app?.service("carts").find({
      query: {
        fields: ["id", "user_id"],
        filter: {
          user_id: {
            _eq: vefiryResult.issuer,
          },
        },
      },
    });
    console.log(result.data);
    if (result.data.length !== 0) {
      cart = result.data[0];
    } else {
      const createResult = await this.app
        ?.service("carts")
        .create({ user_id: vefiryResult.issuer });
      cart = createResult;
    }
    return {
      authentication: { strategy: this.name },
      user: {
        _id: vefiryResult.issuer,
        cart,
      },
      app: {
        _id: this.app?.get("natadecoco_id"),
      },
      env: this.app?.get("dev_env"),
    };
  }
}
