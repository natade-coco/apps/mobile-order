import { AuthenticationService } from "@feathersjs/authentication/lib";
import { Application as ExpressFeathers } from "@feathersjs/express";
import { Carts } from "./services/carts/carts.class";
import { Catalogs } from "./services/catalogs/catalogs.class";
import { Hooks } from "./services/hooks/hooks.class";
import { Messages } from "./services/messages/messages.class";
import { Orders } from "./services/orders/orders.class";
import { Products } from "./services/products/products.class";
import { Settings } from "./services/settings/settings.class";

// A mapping of service names to types. Will be extended in service files.
export interface ServiceTypes {
  "/authentication": AuthenticationService;
  "/carts": Carts;
  "/catalogs": Catalogs;
  "/hooks": Hooks;
  "/messages": Messages;
  "/orders": Orders;
  "/products": Products;
  "/settings": Settings;
}
// The application instance type that will be used everywhere else
export type Application = ExpressFeathers<ServiceTypes>;
