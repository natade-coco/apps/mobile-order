import { Cart } from './cart'

interface User {
  id: string | null
  cart: Cart
}

interface App {
  id: string | null
}

export interface Session {
  token: string | null
  user: User | null
  app: App | null
  env: 'prd' | 'stg'
}

export type Id = string | number
