export interface Setting {
  id: number
  dateCreated: string
  dateUpdated: string
  isPrepay: boolean
  taxRate: number
  receptionStartTime: string
  receptionFinishTime: string
  storeName: string
  isIncludedTax: boolean
  stripeAccount?: string
  bannerImage?: string
  bannerBackgroundColor?: string
  bannerTextColor?: string
}
