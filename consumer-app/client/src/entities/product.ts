export type ProductState = 'in_service' | 'stopped'
export interface Product {
  id: number
  title: string
  subtitle: string
  image: string
  status: string
  description: string
  // estimated_time: number
  state: ProductState
  prices: Price[]
  catalogs: { catalogsId: number }[]
}
export interface Price {
  id: number
  dateCreated?: string
  dateUpdated?: string
  amount: number
  caption?: string
  isStandard: boolean
}
