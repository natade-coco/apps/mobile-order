export interface Catalog {
  id: number
  title: string
  orderStartTime: string
  orderStopTime: string
}
