export type OrderState = 'accepted' | 'prepared' | 'served' | 'canceled'
export interface Order {
  id?: number
  dateCreated?: string
  dateUpdated?: string
  state: OrderState
  reference: string
  cart: number
  email?: string
  units: OrderUnit[]
  isRead: boolean
  txId?: string
  chargeId?: string
  receiptUrl?: string
}
export type OrderParams = Omit<Order, 'id' | 'dateCreated' | 'dateUpdated'>

export interface OrderUnit {
  id?: number
  priceId?: number
  title: string
  caption: string
  price: number
  quantity: number
  product: number
  image?: string
}
