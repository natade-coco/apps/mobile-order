import * as React from 'react'

export const OrderState = (props: { state: string; size?: string }) => {
  switch (props.state) {
    case 'accepted':
      return <span className={`tag is-light has-text-weight-bold is-primary ${props.size}`}>準備中</span>
    case 'prepared':
      return <span className={`tag has-text-weight-bold is-light is-warning ${props.size}`}>受取可</span>
    case 'served':
      return <span className={`tag has-text-weight-bold is-light is-white ${props.size}`}>受取済</span>
    case 'canceled':
      return <span className={`tag has-text-weight-bold is-light is-danger ${props.size}`}>キャンセル済</span>
    default:
      return <span className={`tag is-light ${props.size}`}>不明</span>
  }
}
