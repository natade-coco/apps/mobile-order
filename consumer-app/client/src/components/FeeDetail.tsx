import * as React from 'react'
import { OrderUnit } from '../entities/order'

interface FeeDetailProps {
  units: OrderUnit[]
  taxRate: number
}

export const FeeDetail = (props: FeeDetailProps) => {
  const totalCnt = props.units?.reduce((acc, val) => acc + val.quantity, 0)
  const tax = props.units?.reduce(
    (acc, val) => acc + Math.floor((val.price / (1 + props.taxRate / 100)) * (props.taxRate / 100)) * val.quantity,
    0
  )
  const total = props.units?.map((unit) => unit.quantity * unit.price).reduce((acc, val) => acc + val, 0)
  const subtotal = props.units?.reduce((acc, val) => acc + val.price * val.quantity, 0)

  return (
    <table className="table is-fullwidth">
      <tbody>
        <tr>
          <td>小計</td>
          <td>{totalCnt} 点</td>
          <td className="has-text-right" style={{ minWidth: '7rem' }}>
            ¥ {subtotal?.toLocaleString()}
          </td>
        </tr>
        <tr>
          <td>(内消費税</td>
          <td></td>
          <td className="has-text-right">¥ {tax?.toLocaleString()})</td>
        </tr>
        <tr>
          <td>
            合計金額<span className="is-size-7">(税込)</span>
          </td>
          <td></td>
          <td className="has-text-right" style={{ verticalAlign: 'text-bottom' }}>
            ¥ <strong className="is-size-4 has-text-danger">{total?.toLocaleString()}</strong>
          </td>
        </tr>
      </tbody>
    </table>
  )
}
