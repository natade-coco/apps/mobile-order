import * as React from 'react'
import { OrderUnit } from '../entities/order'

interface OrderUnitProps {
  unit: OrderUnit
  editable: boolean
  disabled: boolean
  useTitle: boolean
  useImage?: boolean
  increaseItem?: (id: number, priceId: number) => void
  decreaseItem?: (id: number, priceId: number) => void
}

export const OrderUnitItem = (props: OrderUnitProps) => {
  return (
    <div className="columns mb-3" style={{}}>
      <div className="column py-0 is-flex is-align-items-center">
        {props.useImage && (
          <img
            className="order-list-image mt-1"
            src={props.unit.image + '?key=icon'}
            style={{
              height: '2rem',
              width: '2rem',
              objectFit: 'cover',
              display: 'inline',
              borderRadius: '999px',
              marginRight: '0.5rem'
            }}
          />
        )}
        {props.useTitle ? (
          <span>
            {props.unit.title}
            <span style={{ fontSize: '0.85rem' }}>
              {props.unit.caption && props.useTitle ? ` (${props.unit.caption})` : props.unit.caption}
            </span>
          </span>
        ) : null}
      </div>
      <div className="column py-0">
        <div className="columns is-mobile is-vcentered">
          <div className="column is-two-third">
            ¥ <span className="is-size-4 has-text-weight-bold">{Math.floor(props.unit.price).toLocaleString()}</span>
          </div>
          <div className="column is-one-thirds has-text-right">
            <nav className="level is-mobile">
              {props.disabled ? null : props.editable ? (
                <CountDown disabled={props.unit.quantity === 0} unit={props.unit} fn={props.decreaseItem} />
              ) : null}
              <Count disabled={!props.editable} quantity={props.unit.quantity} />
              {props.disabled ? null : props.editable ? <CountUp unit={props.unit} fn={props.increaseItem} /> : null}
            </nav>
          </div>
        </div>
      </div>
    </div>
  )
}

const CountDown = (props: { unit: OrderUnit; fn: (id: number, priceId: number) => void; disabled: boolean }) => (
  <div
    className="level-item has-text-centered"
    onClick={() => props.fn(props.unit.id, props.unit.priceId)}
    style={props.disabled ? { opacity: 0.2, pointerEvents: 'none' } : {}}
  >
    <span className="icon mx-3" style={{ color: '#a3a3a3' }}>
      <i className="fa fa-minus-circle" aria-hidden="true"></i>
    </span>
  </div>
)

const Count = (props: { quantity: Number; disabled: boolean }) => (
  <div className="level-item has-text-centered">
    <span className="has-text-centered" style={{ width: '2.5rem', whiteSpace: 'nowrap' }}>
      {props.quantity} {props.disabled && ' 点'}
    </span>
  </div>
)

const CountUp = (props: { unit: OrderUnit; fn: (id: number, priceId: number) => void }) => {
  return (
    <div className="level-item has-text-centered" onClick={() => props.fn(props.unit.id, props.unit.priceId)}>
      <span className="icon mx-3" style={{ color: '#ffc600' }}>
        <i className="fa fa-plus-circle" aria-hidden="true"></i>
      </span>
    </div>
  )
}
