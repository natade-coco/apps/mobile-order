import * as React from 'react'
import { Link } from '@reach/router'

export const OrderNotExists = () => (
  <article className="message is-size-7 mx-3">
    <div className="message-body">
      ようこそ！<Link to="/">コチラ</Link> から商品をご注文ください。
    </div>
  </article>
)
