import * as React from 'react'
import { OrderState } from '../components/OrderState'
import { Order } from '../entities/order'

interface OrderResultProps {
  show: boolean
  order?: Order
  goBack: () => void
  goNext: () => void
}

export const OrderResult = (props: OrderResultProps) => {
  return (
    <div className={`modal ${props.show ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head" style={{ background: 'white' }}>
          <p className="modal-card-title">
            No.<span className="is-size-1 has-text-weight-medium">{props.order?.reference}</span>
          </p>
          <OrderState state={props.order?.state} size={'is-large'} />
        </header>
        <section className="modal-card-body px-0 py-0">
          <div className="card">
            <div className="card-content px-5 py-2">
              <div className="container py-3">
                <div className="level">
                  <div className="level-left">
                    <div className="level-item has-text-centered">
                      <span className="icon has-text-primary is-large">
                        <i className="fas fa-2x fa-check-circle" aria-hidden="true"></i>
                      </span>
                      <p className="is-size-5">ご注文ありがとうございます</p>
                    </div>
                  </div>
                </div>
                <p>
                  ご注文が <OrderState state={'prepared'} />{' '}
                  になりましたらカウンターにおこしいただき、注文履歴をご提示ください。
                </p>
              </div>
            </div>
          </div>
        </section>
        <footer className="modal-card-foot has-background-white">
          <div className="container">
            <button className="button is-pulled-left" onClick={props.goBack}>
              とじる
            </button>
            <button
              className="button is-pulled-right is-warning has-text-white has-text-weight-bold"
              onClick={props.goNext}
            >
              注文履歴
            </button>
          </div>
        </footer>
      </div>
    </div>
  )
}
