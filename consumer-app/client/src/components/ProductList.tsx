import * as React from 'react'
import { ProductNotExists } from '../components/ProductNotExists'
import { Product } from '../entities/product'
import defaultImage from '../images/item-default.png'
import ContentLoader from './ContentLoader'

interface ProductListProps {
  products: Product[]
  onSelect: (productId: number) => void
  existCatalog: boolean
  isLoading: boolean
}

export const ProductList = (props: ProductListProps) => {
  return (
    <div
      className="section px-3"
      style={{ overflow: 'scroll', paddingTop: `${props.existCatalog ? 4 : 2}rem`, paddingBottom: '7rem' }}
    >
      {props.isLoading ? (
        <ContentLoader />
      ) : props.products.length > 0 && props.products.filter((i) => i.prices?.length > 0).length > 0 ? (
        <ProductListTable products={props.products} onSelect={props.onSelect} />
      ) : (
        <ProductNotExists existCatalog={props.existCatalog} />
      )}
    </div>
  )
}

interface ProductListTableProps {
  products: Product[]
  onSelect: (productId: number) => void
}

const ProductListTable = (props: ProductListTableProps) => {
  const listItems = props.products.map((product) => (
    <ProductListItem key={product.id} product={product} onSelect={props.onSelect} />
  ))

  return <div className="columns is-mobile is-multiline">{listItems}</div>
}

interface ProductListItemProps {
  product: Product
  onSelect: (productId: number) => void
}

const ProductListItem = (props: ProductListItemProps) => {
  // 標準価格
  const standardPrice = props.product?.prices.find((p) => p.isStandard) || props.product?.prices[0]

  return (
    <div className="column is-half" style={{ position: 'relative' }}>
      {props.product.state === 'stopped' && (
        <div
          style={{
            background: '#7e807ecc',
            position: 'absolute',
            top: '0.75rem',
            bottom: '0.75rem',
            right: '0.75rem',
            left: '0.75rem',
            zIndex: 1,
            borderRadius: '0.5rem',
            textAlign: 'center',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center'
          }}
        >
          <p style={{ color: 'white', fontWeight: 'bold', fontSize: '1rem' }}>
            本日の販売は、
            <br />
            終了いたしました。
          </p>
        </div>
      )}
      <div
        className="card is-flex is-flex-direction-column is-justify-content-space-between product-list-item"
        style={{ borderRadius: '0.5em', height: '100%' }}
        onClick={() => props.onSelect(props.product.id)}
      >
        <div
          className="card-image has-text-centered is-flex is-flex-direction-column is-justify-content-center"
          style={{ height: '100%' }}
        >
          <img
            style={{
              borderRadius: '0.5em 0.5em 0 0',
              objectFit: 'cover',
              height: 'auto',
              maxHeight: '',
              width: '100%',
              maxWidth: '47vw',
              display: 'block'
            }}
            src={props.product?.image ? `${props.product.image}` : defaultImage}
            alt="product"
          />
        </div>
        <div className="card-content px-2 py-2">
          <div className="container" style={{ minHeight: '1.5em' }}>
            <strong>{props.product.title}</strong>
            <span className="is-size-7 pl-1" style={{ verticalAlign: 'middle' }}>
              {standardPrice?.caption ? `(${standardPrice.caption})` : ''}
            </span>
          </div>
          <span style={{ minHeight: '40px' }}>
            ¥{' '}
            <strong className="is-size-4">
              {Math.floor(standardPrice?.amount || props.product.prices[0].amount).toLocaleString()}
            </strong>
          </span>
        </div>
      </div>
    </div>
  )
}
