import * as React from 'react'
import { Router, Location } from '@reach/router'

export const DefaultRouter = (props: any) => (
  <Location>{({ location }) => <Router location={location}>{props.children}</Router>}</Location>
)
