import * as React from 'react'
import moment from 'moment'
import { OrderState } from '../components/OrderState'
import { OrderNotExists } from '../components/OrderNotExists'
import { navigate } from 'gatsby'
import leftArrow from '../images/left-arrow.svg'
import { useEffect } from 'react'
import { getDiffTime } from '../util'
import { useState } from 'react'
import ContentLoader from './ContentLoader'
import { Order, OrderState as State } from '../entities/order'

interface OrderListProps {
  orders: Order[]
  onSelected: (order_id: number) => void
  isLoading: boolean
}

export const OrderList = (props: OrderListProps) => {
  return (
    <div className="">
      <div
        className="columns is-mobile m-0 has-background-white"
        style={{ position: 'fixed', width: '100%', zIndex: 1, height: '50px' }}
      >
        <div className="column is-2 is-narrow is-flex is-align-items-center">
          <div>
            <div
              style={{
                display: 'flex',
                height: '1.8rem',
                width: '1.8rem',
                border: '1px solid #aaa',
                borderRadius: '50%',
                justifyContent: 'center',
                flexDirection: 'column'
              }}
              onClick={() => {
                navigate('/')
              }}
            >
              <img className="is-rounded" src={leftArrow} style={{ height: '1.3rem' }} />
            </div>
          </div>
        </div>
        <div className="column is-8 is-narrow has-text-centered has-text-weight-bold is-size-6 is-flex is-flex-direction-column is-justify-content-center">
          注文履歴
        </div>
      </div>
      <div style={{ paddingTop: '3.5rem' }}>
        {props.isLoading ? (
          <ContentLoader />
        ) : props.orders.length > 0 ? (
          <OrderListTable orders={props.orders} onSelected={props.onSelected} />
        ) : (
          <OrderNotExists />
        )}
      </div>
    </div>
  )
}

interface OrderListTableProps {
  orders?: Order[]
  onSelected: (order_id: number) => void
}

const OrderListTable = (props: OrderListTableProps) => {
  const listItems = props.orders
    ?.slice()
    .sort((a, b) => moment(b.dateCreated || b.dateUpdated).diff(a.dateCreated || a.dateUpdated))
    .map((order) => <OrderListItem key={order.id} order={order} onSelected={props.onSelected} />)

  return (
    <div className="container section px-3 pb-5" style={{ paddingTop: '0rem' }}>
      {listItems}
    </div>
  )
}

interface OrderListItemProps {
  order: Order
  onSelected: (order_id: number) => void
}

const OrderListItem = (props: OrderListItemProps) => {
  const [time, setTime] = useState('')
  useEffect(() => {
    setTime(getDiffTime(props.order.dateUpdated || props.order.dateCreated))
    const time = setInterval(() => {
      setTime(getDiffTime(props.order.dateUpdated || props.order.dateCreated))
    }, 1000 * 10)
    return () => clearInterval(time)
  }, [props.order])

  const actionText = (state: State) => {
    switch (state) {
      case 'accepted':
        return '注文しました'
      case 'prepared':
        return '出来上がりました'
      case 'served':
        return '受取りました'
      case 'canceled':
        return 'キャンセルされました'
      default:
        return ''
    }
  }

  const actionColor = (state: State) => {
    switch (state) {
      case 'accepted':
        return '#59c5d2'
      case 'prepared':
        return '#ffbf63'
      case 'served':
        return '#9e9e9e'
      case 'canceled':
        return '#ff7373'
      default:
        return '#000'
    }
  }

  return (
    <article className="message order-list-item">
      <div className="message-header py-2 has-text-white" style={{ background: actionColor(props.order.state) }}>
        <small>
          <i className="far fa-clock" aria-hidden="true"></i>
          <span style={{ marginLeft: '0.2rem' }}>
            {time}に{actionText(props.order.state)}
          </span>
        </small>
      </div>
      <div className="message-body box pl-4 pr-0 py-1">
        <article
          className="media is-align-items-center order-list-item-body"
          style={{ minHeight: '3.2rem' }}
          onClick={() => props.onSelected(props.order.id)}
        >
          <div className="media-content">
            <div className="content is-flex is-align-items-center">
              <span style={{ marginRight: '0.2rem' }}>
                No.<strong className="is-size-3">{props.order.reference}</strong>
              </span>
              <span className="is-flex is-align-items-center is-flex-direction-row-reverse">
                {props.order.units
                  .slice(0, 4)
                  .map((item, key) => (
                    <img
                      className="order-list-image"
                      key={`order-list-image-${key}`}
                      src={item.image + '?key=icon'}
                      style={{
                        height: '2rem',
                        width: '2rem',
                        objectFit: 'cover',
                        display: 'inline',
                        borderRadius: '999px',
                        marginLeft: '-1rem'
                      }}
                    />
                  ))
                  .reverse()}
              </span>
            </div>
          </div>
          <div className="media-content">
            <div className="content has-text-right">
              <OrderState state={props.order.state} size="is-medium" />
            </div>
          </div>
          <div className="media-right" style={{ margin: 'auto 0.75rem' }}>
            <i className="fas fa-info-circle" style={{ fontSize: '1rem', color: '#929292' }}></i>
          </div>
        </article>
      </div>
    </article>
  )
}
