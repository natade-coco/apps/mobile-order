import * as React from 'react'

export const ProductNotExists = (props: { existCatalog: boolean }) => (
  <article className="message is-size-7">
    <div className="message-body">
      {props.existCatalog ? (
        <span>商品がありません。</span>
      ) : (
        <span>
          ようこそ！
          <br />
          商品を登録してモバイルオーダーをはじめましょう。
        </span>
      )}
    </div>
  </article>
)
