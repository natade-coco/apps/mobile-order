import * as React from 'react'
import { Catalog } from '../entities/catalog'
import { isInTime } from '../util'

interface CatalogListProps {
  catalogs: Catalog[]
  currentCatalog: number
  onSelect: (catalogId: number) => void
}

export const CatalogList = (props: CatalogListProps) => {
  const listItems = props.catalogs.map((catalog) => (
    <CatalogListItem
      key={catalog.id}
      catalog={catalog}
      currentCatalog={props.currentCatalog}
      onSelect={props.onSelect}
    />
  ))

  return (
    <div className="tabs" style={{ zIndex: 1, background: 'white', width: '100%', position: 'fixed' }}>
      <ul>{listItems}</ul>
    </div>
  )
}

interface CatalogListItemProps {
  catalog: Catalog
  currentCatalog: number
  onSelect: (catalogId: number) => void
}

const CatalogListItem = (props: CatalogListItemProps) => {
  const startTime = props.catalog.orderStartTime
  const endTime = props.catalog.orderStopTime
  return (
    <>
      {isInTime(startTime, endTime) && (
        <li className={props.catalog.id === props.currentCatalog ? 'is-active' : ''}>
          <a
            className={`${props.catalog.id === props.currentCatalog ? 'has-text-weight-medium' : ''}`}
            style={{ height: '50px' }}
            onClick={() => {
              props.onSelect(props.catalog.id)
            }}
          >
            {props.catalog.title}
          </a>
        </li>
      )}
    </>
  )
}
