import * as React from 'react'
import { OrderState } from '../components/OrderState'
import { FeeDetail } from '../components/FeeDetail'
import { OrderUnitItem } from '../components/OrderUnit'
import { Order } from '../entities/order'

interface OrderDetailProps {
  order?: Order
  taxRate: number
  onTax: boolean
  goBack: () => void
  show: boolean
}

export const OrderDetail = (props: OrderDetailProps) => {
  const listItems = props.order?.units.map((unit) => (
    <OrderUnitItem useImage={true} key={unit.id} unit={unit} editable={false} disabled={false} useTitle={true} />
  ))
  return (
    <div className={`modal ${props.order ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head" style={{ background: '#f3f3f3', border: 'none' }}>
          <p className="modal-card-title">
            No.<span className="is-size-1 has-text-weight-medium">{props.order?.reference}</span>
          </p>
          <OrderState state={props.order?.state} size={'is-large'} />
        </header>
        <section className="modal-card-body px-0 py-0">
          <div className="card" style={{ overflowX: 'hidden' }}>
            <div className="card-content px-2 py-2">
              <div className="container px-2 pt-5 pb-0">
                <div className="columns">
                  <div className="column">{listItems}</div>
                </div>
                <div className="columns"></div>
              </div>
              <div className="container py-3">
                <section className="hero is-light">
                  <FeeDetail units={props.order?.units} taxRate={props.taxRate} />
                </section>
              </div>
            </div>
          </div>
        </section>
        <footer className="modal-card-foot" style={{ background: 'white' }}>
          <div className="container has-text-centered">
            <button className="button" onClick={() => props.goBack()}>
              とじる
            </button>
          </div>
        </footer>
      </div>
    </div>
  )
}
