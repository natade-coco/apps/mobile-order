import * as React from 'react'

export type CartButtonProps = {
  opacity?: number
  number?: number
  onClick: () => void
}

const CartButton = (props: CartButtonProps) => {
  return (
    <div className="cart-btn" style={{ opacity: props.opacity }} onClick={props.onClick}>
      {props.number > 0 && (
        <div className="badge">
          <span>{props.number}</span>
        </div>
      )}
      <span className="fa-stack">
        <i className="fas fa-circle fa-stack-2x" style={{ color: '#ffc600', fontSize: '2.3rem' }}></i>
        <i
          className="fas fa-shopping-cart fa-stack-1x fa-inverse"
          style={{ fontSize: '2rem', transform: 'translateX(-2px)' }}
        ></i>
      </span>
    </div>
  )
}

export default CartButton
