import * as React from 'react'
import defaultImage from '../images/item-default.png'
import { useState } from 'react'
import { Product } from '../entities/product'
import { OrderUnit } from '../entities/order'

interface ProductDetailProps {
  product?: Product
  addToCart: (unit: OrderUnit[]) => void
  goBack: () => void
  show: boolean
}

export const ProductDetail = (props: ProductDetailProps) => {
  const [orderUnits, setOrderUnits] = useState<OrderUnit[]>([])
  const [zoomImg, setZoomImg] = useState(false)
  const addCart = (unit: OrderUnit) => {
    const newOrderUnits = [...orderUnits]
    const target = newOrderUnits.find((i) => i.priceId === unit.priceId)
    if (target) {
      target.quantity += 1
    } else {
      unit.quantity += 1
      newOrderUnits.push(unit)
    }
    setOrderUnits(newOrderUnits)
  }
  const removeCart = (unit: OrderUnit) => {
    let newOrderUnits = [...orderUnits]
    const target = newOrderUnits.find((i) => i.priceId === unit.priceId)
    if (target) {
      target.quantity -= 1
      if (target.quantity === 0) {
        newOrderUnits = newOrderUnits.filter((i) => i.priceId !== unit.priceId)
      }
    } else return
    setOrderUnits(newOrderUnits)
  }

  const listItems = props.product?.prices
    .map((price) => {
      return {
        id: props.product.id,
        priceId: price.id,
        price: price.amount,
        title: props.product.title,
        caption: price.caption,
        quantity: orderUnits.find((i) => i.priceId === price.id)?.quantity || 0,
        product: props.product.id,
        image: props.product.image
      }
    })
    .map((unit) => (
      <article key={`${unit.id}-${unit.priceId}`} className="message order-list-item">
        <div className="message-header py-1 has-text-whit px-1" style={{ background: '#64c6af' }}>
          <small>
            <span style={{ marginLeft: '0.2rem' }}>{unit.caption}</span>
          </small>
        </div>
        <div className="message-body box pl-2 pr-0 py-1">
          <article className="media is-align-items-center" style={{ minHeight: '3.2rem' }}>
            <div className="media-content">
              <div className="content is-flex is-align-items-center">
                <span style={{ marginRight: '0.2rem' }}>
                  ¥ <strong className="is-size-4">{unit.price.toLocaleString()}</strong>
                </span>
              </div>
            </div>
            <div>
              <nav className="level is-mobile">
                <CountDown disabled={unit.quantity === 0} fn={removeCart} unit={unit} />
                <Count quantity={unit.quantity} />
                <CountUp fn={addCart} unit={unit} />
              </nav>
            </div>
          </article>
        </div>
      </article>
    ))

  return (
    <div className={`modal ${props.show ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head is-block" style={{ border: 'none', background: '#53b6c0' }}>
          <div className="has-text-weight-medium is-size-5">
            <span className="" style={{ color: '#fff' }}>
              {props.product?.title}
            </span>
          </div>
          <div className="is-size-7" style={{ color: '#fff' }}>
            {props.product?.subtitle}
          </div>
        </header>
        <section className="modal-card-body px-0 py-0">
          <div className="columns is-mobile" style={{ margin: 0, minHeight: '20vh' }}>
            <div className="column is-two-fifths" style={{}}>
              <div className="" style={{ borderRadius: 0 }}>
                <figure
                  className="image is-flex is-align-items-center is-justify-content-center has-background-black"
                  style={{ borderRadius: '0.3rem' }}
                >
                  <img
                    onClick={() => setZoomImg(true)}
                    src={props.product?.image ? `${props.product.image}?key=card` : defaultImage}
                    style={{
                      objectFit: 'cover',
                      maxHeight: '300px',
                      height: 'auto',
                      maxWidth: '100vw',
                      width: '100%',
                      borderRadius: '0.3rem'
                    }}
                  />
                  <div style={{ position: 'absolute', bottom: '3px', right: '3px', color: '#fff' }}>
                    <i className="fas fa-search-plus"></i>
                  </div>
                </figure>
                {props.product?.description && (
                  <div className="mt-4" style={{ whiteSpace: 'break-spaces', fontSize: '0.8rem' }}>
                    <p>{props.product?.description}</p>
                  </div>
                )}
              </div>
            </div>
            <div className="column is-three-fifths" style={{ overflow: 'hidden' }}>
              <div className="">{listItems}</div>
            </div>
          </div>
        </section>
        <footer className="modal-card-foot" style={{ background: '#fff' }}>
          <div className="container">
            <button
              className="button is-pulled-left"
              onClick={() => {
                props.goBack()
                setOrderUnits([])
              }}
              style={{}}
            >
              とじる
            </button>
            <button
              className="button is-pulled-right is-warning has-text-white"
              disabled={!(orderUnits.length > 0)}
              onClick={() => {
                props.addToCart(orderUnits)
                setOrderUnits([])
              }}
            >
              <strong>カートに入れる</strong>
            </button>
          </div>
        </footer>
      </div>
      <div className={`modal ${zoomImg ? 'is-active' : ''}`}>
        <div className="modal-background" onClick={() => setZoomImg(false)}></div>
        <div className="modal-content">
          <button
            className="delete"
            aria-label="close"
            style={{ float: 'right', marginRight: '0.5rem', marginBottom: '0.5rem', zIndex: 2 }}
            onClick={() => setZoomImg(false)}
          ></button>
          <p className="image">
            <img src={props.product?.image} style={{}} />
          </p>
        </div>
      </div>
    </div>
  )
}

const CountDown = (props: { unit: OrderUnit; fn: (ou: OrderUnit) => void; disabled: boolean }) => (
  <div
    className=""
    onClick={() => props.fn(props.unit)}
    style={props.disabled ? { opacity: 0.2, pointerEvents: 'none' } : {}}
  >
    <span className="icon mx-1" style={{ color: '#a3a3a3' }}>
      <i className="fa fa-minus-circle" aria-hidden="true"></i>
    </span>
  </div>
)

const Count = (props: { quantity: Number }) => (
  <div className="mx-1 has-text-centered" style={{ minWidth: '1.4rem' }}>
    <span className="" style={{ whiteSpace: 'nowrap' }}>
      {props.quantity}
    </span>
  </div>
)

const CountUp = (props: { unit: OrderUnit; fn: (ou: OrderUnit) => void }) => (
  <div className="" onClick={() => props.fn(props.unit)}>
    <span className="icon mx-1" style={{ color: '#ffc600' }}>
      <i className="fa fa-plus-circle" aria-hidden="true"></i>
    </span>
  </div>
)
