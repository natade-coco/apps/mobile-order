import * as React from 'react'

interface OrderStopProps {}

export const OrderStop = (props: OrderStopProps) => {
  return (
    <div>
      <div
        style={{
          width: '100vw',
          height: '94vh',
          background: 'grey',
          textAlign: 'center',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center'
        }}
      >
        <span style={{ color: 'white', fontWeight: 'bold', fontSize: '2rem' }}>
          本日の営業は
          <br />
          終了いたしました
        </span>
      </div>
    </div>
  )
}
