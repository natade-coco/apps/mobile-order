import * as React from 'react'
import { Payout } from '@natade-coco/react-pay-js'
import { FeeDetail } from '../components/FeeDetail'
import { OrderUnitItem } from '../components/OrderUnit'
import { OrderUnit } from '../entities/order'

interface CartDetailProps {
  isLoading: boolean
  showCart: boolean
  unsetteled: OrderUnit[]
  taxRate: number
  onTax: boolean
  isPrepay: boolean
  increaseItem: (id: number, priceId: number) => void
  decreaseItem: (id: number, priceId: number) => void
  resetCart: () => void
  goBack: () => void
  goNext: () => void
  onCompleted: (txId: string, status: string, chargeId: string, receiptUrl: string) => boolean
  onError: (reason: any) => void
  account: string
}

export const CartDetail = (props: CartDetailProps) => {
  const amount = props.unsetteled.map((unit) => unit.quantity * unit.price).reduce((acc, val) => acc + val, 0)
  const listItems = props.unsetteled.map((unit) => (
    <OrderUnitItem
      key={unit.priceId}
      unit={unit}
      editable={true}
      disabled={props.isLoading}
      useTitle={true}
      useImage={true}
      decreaseItem={props.decreaseItem}
      increaseItem={props.increaseItem}
    />
  ))

  const totalCnt = props.unsetteled.map((unit) => unit.quantity).reduce((acc, val) => acc + val, 0)
  const isPayable = amount >= 50

  return (
    <div className={`modal ${props.showCart ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head" style={{ border: 'none', background: '#53b6c0' }}>
          <p className="modal-card-title is-size-5 has-text-white has-text-weight-medium">カート内の商品</p>
          <button
            className="button is-light is-small is-danger"
            disabled={props.isLoading}
            onClick={() => props.resetCart()}
          >
            <span className="icon is-small">
              <i className="fas fa-trash"></i>
            </span>
            <span className="has-text-weight-bold">全て取消</span>
          </button>
        </header>
        <section className="modal-card-body px-0 py-0">
          {listItems.length > 0 ? (
            <div className="card" style={{ overflowX: 'hidden' }}>
              <div className="card-content px-3 pb-0">
                <div className="container">
                  <div className="columns">
                    <div className="column">{listItems}</div>
                  </div>
                  <div className="columns"></div>
                </div>
                <div className="container py-3">
                  <section className="hero is-light">
                    <FeeDetail units={props.unsetteled} taxRate={props.taxRate} />
                  </section>
                </div>
              </div>
            </div>
          ) : (
            <div
              className="is-flex is-flex-direction-row is-align-items-center is-justify-content-center"
              style={{ height: '30vh' }}
            >
              <span>カート内に商品がありません。</span>
            </div>
          )}
        </section>
        <footer className="modal-card-foot" style={{ background: 'white' }}>
          <div className="container is-flex is-justify-content-space-around">
            <button className="button is-pulled-left" disabled={props.isLoading} onClick={() => props.goBack()}>
              とじる
            </button>
            {props.isPrepay ? (
              <Payout
                theme="light" // option: light(default), dark, inverted
                layout="short"
                account={props.account}
                style={{
                  fontWeight: 900,
                  marginRight: '0px',
                  opacity: isPayable ? 1 : 0.4,
                  pointerEvents: isPayable ? 'all' : 'none'
                }}
                amount={amount}
                opts={{
                  test: true,
                  identityHubToken: process.env.GATSBY_APP_IDENTITYHUB_JWT || undefined,
                  paymentHubToken: process.env.GATSBY_APP_PAYMENTHUB_JWT || undefined
                }}
                onCompleted={props.onCompleted}
                onError={props.onError}
              />
            ) : (
              <button
                className={`button is-primary is-pulled-right ${props.isLoading ? 'is-loading' : ''}`}
                disabled={totalCnt === 0 ? true : false}
                onClick={() => props.goNext()}
              >
                <strong>注文を確定する</strong>
              </button>
            )}
          </div>
        </footer>
      </div>
    </div>
  )
}
