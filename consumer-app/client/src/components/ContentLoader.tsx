import React from 'react'
import ClipLoader from 'react-spinners/ClipLoader'

const Loader = () => (
  <div className="has-text-centered">
    <div className="container" style={{ flex: '0 0 auto' }}>
      <ClipLoader size={25} color={'#09b4c6'} loading={true} />
    </div>
  </div>
)

export default Loader
