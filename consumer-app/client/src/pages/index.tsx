import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../app/rootReducer'
import { DefaultLayout } from '../components/Layout'
import { DefaultRouter } from '../components/Router'
import ErrorModal from '../components/Error'

import Navbar from '../features/consumer/Navbar'
import { Feature as Menu } from '../features/consumer/Menu'
import { Feature as Orders } from '../features/consumer/Orders'
import HashLoader from 'react-spinners/HashLoader'
import './index.scss'

import { OrderStop } from '../components/OrderStop'
import { isInTime } from '../util'
import {
  sessionSelector,
  appStart,
  openError,
  errorContentSelector,
  showErrorSelector,
  closeError
} from '../slices/app'
import { AppDispatch } from '../app/store'
import { getOrders, orderSelector, updateOrderState } from '../slices/order'
import { Order } from '../entities/order'
import { getSetting } from '../slices/setting'

const Page = () => {
  const dispatch: AppDispatch = useDispatch()
  const [isAppReady, setIsAppReady] = useState(false)

  const setting = useSelector((state: RootState) => state.setting)
  const session = useSelector(sessionSelector)
  const orders = useSelector(orderSelector.selectAll)
  const errorContent = useSelector(errorContentSelector)
  const showError = useSelector(showErrorSelector)

  const {
    receptionStartTime: startTime,
    receptionFinishTime: endTime,
    bannerImage,
    bannerBackgroundColor,
    bannerTextColor,
    storeName
  } = setting

  useEffect(() => {
    if (session === null) {
      const cb = (order: Order) => {
        const { id, state } = order
        dispatch(updateOrderState({ id, state }))
      }
      dispatch(appStart(cb))
        .unwrap()
        .then(({ user }) => {
          dispatch(getOrders({ id: user.cart.id }))
          setIsAppReady(true)
          dispatch(getSetting())
            .unwrap()
            .then(({ isPrepay, stripeAccount }) => {
              if (isPrepay && !stripeAccount) {
                dispatch(
                  openError({ title: 'stripeアカウントが未登録です', message: '管理者にお問い合わせください。' })
                )
              }
            })
        })
    }
  }, [])

  const hasPrepare = orders.filter((item) => item.state === 'prepared').length > 0

  const onCloseErrorModal = () => {
    dispatch(closeError())
  }
  const navbarProps = {
    hasPrepare,
    banner: bannerImage,
    bgColor: bannerBackgroundColor,
    textColor: bannerTextColor,
    storeName
  }
  const errorModalProps = {
    ...errorContent,
    onClose: onCloseErrorModal,
    showNotice: showError
  }
  return (
    <>
      {isAppReady ? (
        <DefaultLayout>
          <Navbar {...navbarProps} />
          {isInTime(startTime, endTime) ? (
            <>
              <DefaultRouter>
                <Menu path="/" />
                <Orders path="/orders" />
              </DefaultRouter>
              <ErrorModal {...errorModalProps} />
            </>
          ) : (
            <OrderStop />
          )}
        </DefaultLayout>
      ) : (
        <Loader />
      )}
    </>
  )
}

const Loader = () => (
  <div className="hero is-fullheight">
    <div className="hero-body">
      <div className="container" style={{ flex: '0 0 auto' }}>
        <HashLoader size={100} color={'#09b4c6'} loading={true} />
      </div>
    </div>
  </div>
)

export default Page
