import React, { useState } from 'react'
import { Link } from '@reach/router'
import { useEffect } from 'react'

export type NavbarProps = {
  hasPrepare: boolean
  banner?: string
  bgColor?: string
  textColor?: string
  storeName?: string
}

export const Navbar = (props: NavbarProps) => {
  const { hasPrepare, banner, bgColor, textColor, storeName } = props
  const [disableList, setDisablelist] = useState(false)
  useEffect(() => {
    setDisablelist(window.location.pathname === '/orders')
  }, [window.location.pathname])

  return (
    <nav
      className="navbar is-fixed-top"
      style={{ background: bgColor || '#53b6c0' }}
      role="navigation"
      aria-label="main navigation"
    >
      <div className="navbar-brand" style={{}}>
        {banner ? (
          <img src={banner} style={{ height: '2rem', display: 'block', margin: 'auto 0.5rem' }} />
        ) : (
          <div className="navbar-item">
            <Link to="/">
              <h6 className="title is-5" style={{ color: textColor || '#fff' }}>
                {storeName}
              </h6>
            </Link>
          </div>
        )}
        {!disableList && (
          <div
            className="navbar-item"
            style={{ marginLeft: 'auto', top: 0, bottom: 0, right: 0, position: 'absolute' }}
          >
            <Link to="/orders">
              {hasPrepare ? (
                <button
                  className="button is-small has-text-white has-text-weight-bold is-rounded"
                  style={{ border: 'none', background: '#ffc600' }}
                >
                  商品を受け取る
                </button>
              ) : (
                <span className="icon" style={{ verticalAlign: 'middle' }}>
                  <i className="fa fa-list has-text-white" style={{ fontSize: '1.5rem' }}></i>
                </span>
              )}
            </Link>
          </div>
        )}
      </div>
    </nav>
  )
}

export default Navbar
