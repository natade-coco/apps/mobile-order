import React, { useEffect, useState, useCallback } from 'react'
import { navigate } from '@reach/router'
import { useSelector, useDispatch } from 'react-redux'
import { CatalogList } from '../../components/CatalogList'
import { ProductList } from '../../components/ProductList'
import { ProductDetail } from '../../components/ProductDetail'
import { CartDetail } from '../../components/CartDetail'
import { OrderResult } from '../../components/OrderResult'
import CartButton from '../../components/CartButton'

import { OrderStorage } from '../../services/localstorage'
import { AppDispatch } from '../../app/store'
import { createOrder, orderSelector } from '../../slices/order'
import { catalogsSelector, getCatalogs, getProducts, productsSelector } from '../../slices/product'
import { Product } from '../../entities/product'
import {
  addCartItems,
  cartItemQuantitySelector,
  cartItemSelector,
  decreaseCartItem,
  increaseCartItem,
  normalizeCartItem,
  resetCartItems,
  setCartItems,
  setCartUser
} from '../../slices/cart'
import { OrderUnit } from '../../entities/order'
import { RootState } from '../../app/rootReducer'
import { isLoadingSelector, openError, sessionSelector } from '../../slices/app'
import { Catalog } from '../../entities/catalog'

const orderStorage = new OrderStorage()

export const Feature = (props: any) => {
  const dispatch: AppDispatch = useDispatch()
  const orders = useSelector(orderSelector.selectAll)
  const setting = useSelector((state: RootState) => state.setting)
  const { isPrepay, isIncludedTax: onTax, taxRate: tax, stripeAccount } = setting
  const catalogs = useSelector(catalogsSelector)
  const products = useSelector(productsSelector)
  const cartItems = useSelector(cartItemSelector)
  const sumQuantity = useSelector(cartItemQuantitySelector)
  const isLoading = useSelector(isLoadingSelector)
  const session = useSelector(sessionSelector)
  const [currentProduct, setCurrentProduct] = useState<number>(null)
  const [currentCatalog, setCurrentCatalog] = useState<number>(null)
  const [targetProducts, setTargetProducts] = useState<Product[]>([])
  const [opacity, setOpacity] = useState(1)
  const [cartModalState, setCartModalState] = useState(false)
  const [productModalState, setProductModalState] = useState(false)
  const [resultModalState, setResultModalState] = useState(false)

  const targetProduct = products.find((p) => p.id === currentProduct)
  const lastOrder = orders.sort((a, b) => (a.id > b.id ? -1 : 1))[0]

  useEffect(() => {
    Promise.all([dispatch(getCatalogs()), dispatch(getProducts())]).then(([{ payload: _c }, { payload: _p }]) => {
      const _catalogs = _c as Catalog[]
      const _products = _p as Product[]
      if (_catalogs.length > 0) {
        setCurrentCatalog(_catalogs[0].id)
        const target = _products.filter((item) => item.catalogs.map((_i) => _i.catalogsId).includes(_catalogs[0].id))
        setTargetProducts(target)
      }
      const { cart } = session.user
      dispatch(setCartUser(cart))
    })
    const order = orderStorage.getOrder()
    order && dispatch(setCartItems(order.items || []))
    window.ontouchmove = () => setOpacity(0.5)
    window.ontouchend = window.ontouchcancel = () => setOpacity(1)
  }, [])

  useEffect(() => {
    setTargetProducts(products.filter((item) => item.catalogs.map((_i) => _i.catalogsId).includes(currentCatalog)))
  }, [currentCatalog])

  const onSelectCatalog = useCallback((catalogId: number) => {
    setCurrentCatalog(catalogId)
  }, [])

  const onSelectProduct = (productId: number) => {
    setCurrentProduct(productId)
    setProductModalState(true)
  }

  const addItems = (items: OrderUnit[]) => {
    dispatch(addCartItems(items))
    setProductModalState(false)
  }

  const increaseItem = (id: number, priceId: number) => {
    dispatch(increaseCartItem({ id, priceId }))
  }
  const decreaseItem = (id: number, priceId: number) => {
    dispatch(decreaseCartItem({ id, priceId }))
  }

  const onGoBackProductDetail = () => {
    setCurrentProduct(null)
    setProductModalState(false)
  }

  const resetCart = () => {
    dispatch(resetCartItems())
    setCartModalState(false)
  }

  const onGoBackCartDetail = () => {
    setCartModalState(false)
    dispatch(normalizeCartItem())
  }

  const onGoNextCartDetail = () => {
    onCompletedPayment()
  }

  const onGoBackOrderResult = () => {
    setResultModalState(false)
  }

  const onGoNextOrderResult = () => {
    setResultModalState(false)
    navigate(`/orders`)
  }

  const onCompletedPayment = (txId?: string, status?: string, chargeId?: string, receiptUrl?: string) => {
    const { items, user } = orderStorage.getOrder()
    const units = items.filter((order) => order.quantity !== 0)
    dispatch(createOrder({ cart: user.id, units, txId, chargeId, receiptUrl })).then(() => {
      dispatch(resetCartItems())
      orderStorage.clearOrder()
      setCartModalState(false)
      setResultModalState(true)
    })
    return true
  }

  const onErrorPayment = (error: any) => {
    console.log({ error })
    dispatch(openError({ title: 'お支払いがキャンセルされました', message: error.message }))
  }

  const onClickCart = () => {
    setCartModalState(true)
  }

  const catalogListProps = { catalogs, currentCatalog, onSelect: onSelectCatalog }
  const productListProps = {
    isLoading,
    products: targetProducts,
    onSelect: onSelectProduct,
    existCatalog: catalogs.length > 0
  }
  const productDetailProps = {
    product: targetProduct,
    addToCart: addItems,
    goBack: onGoBackProductDetail,
    show: productModalState
  }
  const cartDetailProps = {
    isLoading,
    showCart: cartModalState,
    unsetteled: cartItems,
    taxRate: tax,
    onTax,
    isPrepay,
    increaseItem,
    decreaseItem,
    resetCart,
    goBack: onGoBackCartDetail,
    account: stripeAccount,
    onCompleted: onCompletedPayment,
    onError: onErrorPayment,
    goNext: onGoNextCartDetail
  }
  const orderResultPrpos = {
    show: resultModalState,
    order: lastOrder,
    goBack: onGoBackOrderResult,
    goNext: onGoNextOrderResult
  }

  return (
    <div className="container" style={{ height: 'calc(100vh - 3.25rem)' }}>
      <CatalogList {...catalogListProps} />
      <ProductList {...productListProps} />
      <ProductDetail {...productDetailProps} />
      <CartDetail {...cartDetailProps} />
      <OrderResult {...orderResultPrpos} />
      <CartButton opacity={opacity} number={sumQuantity} onClick={onClickCart} />
    </div>
  )
}
