import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { OrderList } from '../../components/OrderList'
import { OrderDetail } from '../../components/OrderDetail'
import { useEffect } from 'react'
import { getOrders, orderSelector, updateOrderRead } from '../../slices/order'
import { isLoadingSelector, sessionSelector } from '../../slices/app'
import { AppDispatch } from '../../app/store'

export const Feature = (props: any) => {
  const dispatch: AppDispatch = useDispatch()

  const [detailModalState, setDetailModalState] = useState(false)
  const [currentOrder, setCurrentOrder] = useState<number>(null)

  const isLoading = useSelector(isLoadingSelector)
  const orders = useSelector(orderSelector.selectAll)
  const setting = useSelector((state: RootState) => state.setting)
  const { receptionStartTime, receptionFinishTime, taxRate, isIncludedTax: onTax } = setting
  const { user } = useSelector(sessionSelector)

  useEffect(() => {
    dispatch(getOrders({ id: user.cart.id, startTime: receptionStartTime, finishTime: receptionFinishTime }))
      .unwrap()
      .then((orders) => {
        const target = orders.filter((item) => item.state === 'prepared' && !item.isRead)
        target.forEach((item) => dispatch(updateOrderRead({ id: item.id, isRead: true })))
      })
  }, [])

  const onOrderSelected = (orderId: number) => {
    setCurrentOrder(orderId)
    setDetailModalState(true)
  }

  const onGoBackOrderDetail = () => {
    setCurrentOrder(null)
    setDetailModalState(false)
  }

  const orderListProps = {
    isLoading,
    orders,
    onSelected: onOrderSelected
  }
  const orderDetailProps = {
    order: orders.filter((p) => p.id == currentOrder)[0],
    taxRate,
    onTax,
    goBack: onGoBackOrderDetail,
    show: detailModalState
  }

  return (
    <div className="container">
      <OrderList {...orderListProps} />
      <OrderDetail {...orderDetailProps} />
    </div>
  )
}
