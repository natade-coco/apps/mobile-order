import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Setting } from '../entities/setting'
import SettingService from '../services/server/setting'

const settingService = new SettingService()

export interface State extends Partial<Setting> {}

const initialState: State = {}

// Selectors

const settingISelector = (state: RootState) => state.setting

// Action
export const getSetting = createAsyncThunk(
  'setting/get',
  async (): Promise<Setting> => {
    try {
      const result = await settingService.getSettings()
      return result
    } catch (err) {
      throw err
    }
  }
)

const slice = createSlice({
  name: 'setteing',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getSetting.fulfilled, (state, { payload }) => {
      Object.assign(state, { ...payload })
    })
  }
})

export default slice.reducer
