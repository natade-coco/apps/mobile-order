import { createAction, createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Order, OrderParams, OrderState } from '../entities/order'
import OrderService from '../services/server/order'

const orderService = new OrderService()

const entityAdapter = createEntityAdapter<Order>()

// Selecotrs
const orderISelector = (state: RootState) => state.order

export const orderSelector = entityAdapter.getSelectors(orderISelector)

// Thunks
export const getOrders = createAsyncThunk(
  'order/getList',
  async (payload?: { id: number; startTime?: string; finishTime?: string }): Promise<Order[]> => {
    try {
      const { id, startTime, finishTime } = payload
      const orders = await orderService.getOrders(id, startTime, finishTime)
      return orders
    } catch (err) {
      throw err
    }
  }
)

export const createOrder = createAsyncThunk(
  'order/create',
  async (payload: Partial<OrderParams>, { getState }): Promise<Order> => {
    try {
      const { cart, units, txId, chargeId, receiptUrl } = payload
      const state = getState() as RootState
      const orders = orderSelector.selectAll(state)
      const ref = Number(cart) + ('00' + (orders?.length || 0)).slice(-2)
      const result = await orderService.createOrder(cart, ref, units, txId, chargeId, receiptUrl)
      return result
    } catch (err) {
      throw err
    }
  }
)

export const updateOrderRead = createAsyncThunk(
  'order/updateRead',
  async (payload: { id: number; isRead: boolean }): Promise<Order> => {
    try {
      const { id, isRead } = payload
      const order = await orderService.updateOrder(id, { isRead })
      return order
    } catch (err) {
      throw err
    }
  }
)
export const updateOrderState = createAction<{ id: number; state: OrderState }>('order/updateState')

// Slice
const slice = createSlice({
  name: 'order',
  initialState: entityAdapter.getInitialState(),
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getOrders.fulfilled, entityAdapter.setAll)
      .addCase(updateOrderState, (state, { payload }) => {
        const entity = state.entities[payload.id]
        entity.state = payload.state
      })
      .addCase(updateOrderRead.fulfilled, (state, { payload }) => {
        const entity = state.entities[payload.id]
        entity.isRead = payload.isRead
      })
      .addCase(createOrder.fulfilled, entityAdapter.upsertOne)
  }
})

export default slice.reducer
