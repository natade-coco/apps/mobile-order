import { createSlice, createSelector, createAction, PayloadAction, AnyAction } from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Cart } from '../entities/cart'
import { OrderUnit } from '../entities/order'
import { OrderStorage } from '../services/localstorage'

const orderStorage = new OrderStorage()

export interface State {
  user?: Cart
  items: OrderUnit[]
}

const initialState: State = {
  items: []
}

// Selector
const cartISelector = (state: RootState) => state.cart
export const cartUserSelector = createSelector(cartISelector, (state) => state.user)
export const cartItemSelector = createSelector(cartISelector, (state) => state.items)
export const cartItemQuantitySelector = createSelector(cartItemSelector, (items) =>
  items.map((item) => item.quantity).reduce((acc, val) => acc + val, 0)
)

// Action
export const setCartUser = createAction<Cart>('cart/setUser')
export const setCartItems = createAction<OrderUnit[]>('cart/setItems')
export const addCartItems = createAction<OrderUnit[]>('cart/addItem')
export const resetCartItems = createAction('cart/resetItems')
export const increaseCartItem = createAction<{ id: number; priceId: number }>('cart/increaseItem')
export const decreaseCartItem = createAction<{ id: number; priceId: number }>('cart/decreaseItem')
export const normalizeCartItem = createAction('cart/normalizeItem')

// Slice
const slice = createSlice({
  name: 'cart',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(setCartUser, (state, { payload }) => {
        state.user = payload
      })
      .addCase(setCartItems, (state, { payload }) => {
        state.items = payload
      })
      .addCase(resetCartItems, (state) => {
        state.items = []
      })
      .addCase(addCartItems, (state, { payload }) => {
        payload.forEach((unit) => {
          const target = state.items.find((item) => item.id === unit.id && item.priceId === unit.priceId)
          if (target) {
            target.quantity += unit.quantity
          } else {
            state.items.push(unit)
          }
        })
      })
      .addCase(increaseCartItem, (state, { payload }) => {
        const target = state.items.find((item) => item.id === payload.id && item.priceId === payload.priceId)
        target.quantity += 1
      })
      .addCase(decreaseCartItem, (state, { payload }) => {
        const target = state.items.find((item) => item.id === payload.id && item.priceId === payload.priceId)
        if (target.quantity <= 0) return
        target.quantity -= 1
      })
      .addCase(normalizeCartItem, (state) => {
        state.items = state.items.filter((item) => item.quantity > 0)
      })
      .addMatcher(
        (action: AnyAction): action is PayloadAction<any> => String(action.type).startsWith('cart/'),
        (state) => {
          orderStorage.setOrder({ user: state.user, items: state.items })
        }
      )
  }
})

export default slice.reducer
