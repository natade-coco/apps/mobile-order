import { createAsyncThunk, createSlice, createSelector } from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Catalog } from '../entities/catalog'
import { Product } from '../entities/product'
import CatalogService from '../services/server/catalog'
import ProductService from '../services/server/product'

const productService = new ProductService()
const catalogService = new CatalogService()

export interface State {
  catalogs: Catalog[]
  products: Product[]
}

const initialState: State = {
  catalogs: [],
  products: []
}

const productISelector = (state: RootState) => state.product
export const productsSelector = createSelector(productISelector, (state) => state.products)
export const catalogsSelector = createSelector(productISelector, (state) => state.catalogs)

export const getProducts = createAsyncThunk(
  'product/get',
  async (): Promise<Product[]> => {
    try {
      const result = await productService.getProducts()
      return result
    } catch (err) {
      throw err
    }
  }
)

export const getCatalogs = createAsyncThunk(
  'product/getCatalogs',
  async (): Promise<Catalog[]> => {
    try {
      const result = await catalogService.getCatalogs()
      return result
    } catch (err) {
      throw err
    }
  }
)

const slice = createSlice({
  name: 'product',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getProducts.fulfilled, (state, { payload }) => {
        state.products = payload
      })
      .addCase(getCatalogs.fulfilled, (state, { payload }) => {
        state.catalogs = payload
      })
  }
})

export default slice.reducer
