import { Session } from '../../entities/app'
import { serverApp } from './index'

export default class AppService {
  getSession = (jwt?: string): Promise<Session> => {
    return serverApp.service('authentication').create({ strategy: 'natadecoco', token: jwt })
  }

  issueNotification = (recipient: string, title: string, message: string, did?: string): Promise<any> => {
    return serverApp.service('messages').create({ recipient, title, message, did })
  }

  subscribeOrder = (cb: (order: any) => void) => {
    serverApp.service('hooks').on('created', cb)
  }
}
