import { Application, FeathersService, Params, Service } from '@feathersjs/feathers/lib'
import moment from 'moment'
import { Order, OrderParams, OrderUnit } from '../../entities/order'
import { serverApp } from './index'

export default class OrderService {
  private service: FeathersService<Application, Service<Order>>
  constructor() {
    this.service = serverApp?.service('orders')
  }

  createOrder = async (
    cartId: number,
    reference: string,
    units: OrderUnit[],
    txId?: string,
    chargeId?: string,
    receiptUrl?: string
  ) => {
    const orderUnits = units.map((item) => {
      const { title, caption, price, quantity, product } = item
      return { title, caption, price, quantity, product }
    })
    const result = await this.service.create({
      state: 'accepted',
      reference,
      cart: cartId,
      units: orderUnits,
      isRead: false,
      txId,
      chargeId,
      receiptUrl
    })
    return result
  }

  getOrders = async (cartId: number) => {
    const params = {
      query: {
        fields: ['*', 'units.*', 'cart.*', 'units.product.*'],
        filter: {}
      }
    }
    const startDate = '$NOW(-1 day)'
    const stopDate = '$NOW'
    params.query.filter = {
      date_created: {
        _between: `${startDate},${stopDate}`
      },
      cart: {
        _eq: cartId
      }
    }

    const result = await this.service.find(params)
    if (result instanceof Array) {
      return result
    }
    return [result]
  }

  updateOrder = async (id: number, params: Partial<OrderParams>): Promise<Order> => {
    const result = await this.service.update(id, params)
    if (result instanceof Array) {
      return result[0]
    }
    return result
  }
}
