import { Application, FeathersService, Service } from '@feathersjs/feathers/lib'
import moment from 'moment'
import { Catalog } from '../../entities/catalog'
import { serverApp } from './index'

export default class CatalogService {
  private service: FeathersService<Application, Service<Catalog>>
  constructor() {
    this.service = serverApp?.service('catalogs')
  }

  getCatalogs = async () => {
    const now = moment().format('HH:mm:ss')
    const result = await this.service.find({
      query: {
        filter: {
          status: {
            _eq: 'published'
          },
          _and: [
            {
              _or: [
                {
                  order_start_time: {
                    _lte: now
                  }
                },
                {
                  order_start_time: {
                    _null: true
                  }
                }
              ]
            },
            {
              _or: [
                {
                  order_stop_time: {
                    _gte: now
                  }
                },
                {
                  order_stop_time: {
                    _null: true
                  }
                }
              ]
            }
          ]
        }
      }
    })
    if (result instanceof Array) {
      return result
    }
    return [result]
  }
}
