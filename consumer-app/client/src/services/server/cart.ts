import { Application, FeathersService, Params, Service } from '@feathersjs/feathers/lib'
import { Cart } from '../../entities/cart'
import { serverApp } from './index'

export default class CartService {
  private service: FeathersService<Application, Service<Cart>>
  constructor() {
    this.service = serverApp?.service('carts')
  }

  getCart = async (userId: string): Promise<Cart> => {
    const result = await this.service.find({
      query: {
        fields: ['*', 'orders.*.*', 'orders.units.product.*'],
        filter: {
          userId: {
            _eq: userId
          }
        }
      }
    })
    if (result instanceof Array) {
      return result[0]
    }
    return result
  }

  createCart = async (userId: string): Promise<Cart> => {
    const result = await this.service.create({ userId })
    return result
  }
}
