import { Application, FeathersService, Id, Params, Service } from '@feathersjs/feathers/lib'
import { serverApp } from './index'
import { Product } from '../../entities/product'

export default class ProductService {
  private service: FeathersService<Application, Service<Product>>
  constructor() {
    this.service = serverApp?.service('products')
  }

  getProducts = async () => {
    const result = await this.service.find({
      query: {
        fields: ['*', 'catalogs.*', 'prices.prices_id.*'],
        filter: {
          status: {
            _eq: 'published'
          }
        }
      }
    })
    if (result instanceof Array) {
      return result
    }
    return [result]
  }
}
