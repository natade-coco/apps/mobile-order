import moment from 'moment'
import { Cart } from '../entities/cart'
import { OrderUnit } from '../entities/order'

export interface OrderInfo {
  user: Cart
  items: OrderUnit[]
  created?: string
}

export class OrderStorage {
  KEY: string = 'order'
  storage = typeof window !== 'undefined' ? window.localStorage : undefined
  deleteLimit = 30

  setOrder = (payment: OrderInfo) => {
    payment.created = moment(new Date()).toString()
    const value = JSON.stringify(payment)
    this.storage?.setItem(this.KEY, value)
  }

  getOrder = (): OrderInfo | null => {
    const value = this.storage?.getItem(this.KEY)
    if (!value) return null
    const parsed = JSON.parse(value) as OrderInfo
    if (moment(new Date()).diff(moment(parsed.created), 'minute') > this.deleteLimit) {
      this.clearOrder()
      return null
    }
    return parsed
  }

  clearOrder = () => {
    this.storage?.removeItem(this.KEY)
  }
}
