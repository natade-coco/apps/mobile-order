import { combineReducers } from '@reduxjs/toolkit'
import { TypedUseSelectorHook, useSelector as rawUseSelector } from 'react-redux'
import AppReducer from '../slices/app'
import OrderReducer from '../slices/order'
import ProductReducer from '../slices/product'
import SettingReducer from '../slices/setting'
import CartReducer from '../slices/cart'

const rootReducer = combineReducers({
  app: AppReducer,
  order: OrderReducer,
  product: ProductReducer,
  setting: SettingReducer,
  cart: CartReducer
})

export type RootState = ReturnType<typeof rootReducer>
export const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector

export default rootReducer
