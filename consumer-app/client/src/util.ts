import moment from "moment"
export const isInTime = (startTime?: string, endTime?: string): boolean => {
  if (!startTime || !endTime || startTime === endTime) return true
  const startDate = moment(startTime, "HH:mm")
  const nowDate = moment(new Date())
  const endDate = moment(endTime, "HH:mm")
  if (startDate.isAfter(endDate)) {
    return nowDate.isBetween(startDate, endDate.clone().add(1, "day"), "minute") || nowDate.isBetween(startDate.clone().subtract(1, "day"), endDate, "minute")
  }
  return nowDate.isBetween(startDate, endDate, "minute")
}

export const getDiffTime = (date: string) => {
  const nowMoment = moment()
  const target = moment(date).utc(true).local()
  let diff: any = nowMoment.diff(target, "day")
  let text = "日前"
  if (diff === 0) {
    diff = nowMoment.diff(target, "hour")
    text = "時間前"
    if (diff === 0) {
      diff = nowMoment.diff(target, "minute")
      text = "分前"
      if (diff === 0) {
        diff = nowMoment.diff(target, "second")
        text = "秒前"
      }
    }
  }
  return `${diff}${text}`
}
