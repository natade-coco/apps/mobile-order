import { AuthenticationService } from '@feathersjs/authentication/lib';
import { Application as ExpressFeathers } from '@feathersjs/express';
import { Admins } from './services/admins/admins.class';
import { Messages } from './services/messages/messages.class';
import { Orders } from './services/orders/orders.class';
import { Settings } from './services/settings/settings.class';

// A mapping of service names to types. Will be extended in service files.
export interface ServiceTypes {
  '/admins': Admins;
  '/messages': Messages;
  '/orders': Orders;
  '/settings': Settings;
  '/authentication': AuthenticationService;
}
// The application instance type that will be used everywhere else
export type Application = ExpressFeathers<ServiceTypes>;
