import logger from "./logger";
import app from "./app";

let port = app.get("port");
let server = app.listen(port);

process.on("unhandledRejection", (reason, p) =>
  logger.error("Unhandled Rejection at: Promise ", p, reason)
);

server.then(() =>
  logger.info(
    "Feathers application started on http://%s:%d",
    app.get("host"),
    port
  )
);

server.catch((err) => {
  console.log(err);
  server = app.listen(port++);
});
