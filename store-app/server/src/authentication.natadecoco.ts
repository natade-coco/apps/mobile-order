import { Params } from "@feathersjs/feathers";
import {
  AuthenticationBaseStrategy,
  AuthenticationResult,
  AuthenticationRequest,
} from "@feathersjs/authentication";
import { SDK as Hub, ServiceType } from "@natade-coco/hub-sdk";

export class NatadeCOCOStrategy extends AuthenticationBaseStrategy {
  verifyConfiguration() {
    const config = this.configuration;

    ["tokenField"].forEach((prop) => {
      if (typeof config[prop] !== "string") {
        throw new Error(
          `'${this.name}' authentication strategy requires a '${prop}' setting`
        );
      }
    });
  }

  get configuration() {
    const authConfig = this.authentication?.configuration;
    const config = super.configuration || {};

    return {
      service: authConfig.service,
      entity: authConfig.entity,
      entityId: authConfig.entityId,
      errorMessage: "Invalid token",
      tokenField: config.tokenField,
      ...config,
    };
  }

  async verifyJWT(jwt: string) {
    const id = this.app?.get("natadecoco_id");
    const secret = this.app?.get("natadecoco_secret");
    const client = await Hub.init({
      id,
      secret,
      test: this.app?.get("dev_env") === "stg",
    });
    return client.VerifyJWT(jwt, ServiceType.AppHubService);
  }

  async authenticate(data: AuthenticationRequest, params: Params) {
    console.log({
      devEnv: this.app?.get("dev_env"),
      exec_env: this.app?.get("exec_env"),
    });
    const { tokenField } = this.configuration;
    const token = data[tokenField];
    const vefiryResult =
      this.app?.get("exec_env") === "global"
        ? await this.verifyJWT(token).catch((err) => console.log({ err }))
        : { issuer: "did:ethr:0xf5b82fc83b55bfea06577ce1392fcc9e1e8e6468" };
    console.log({ vefiryResult });
    return {
      authentication: { strategy: this.name },
      user: {
        _id: vefiryResult.issuer,
      },
      app: {
        _id: this.app?.get("natadecoco_id"),
      },
      env: this.app?.get("dev_env"),
    };
  }
}
