import { Service, MemoryServiceOptions } from 'feathers-memory';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';

const COLLECTION_ORDER = 'orders';

export class Orders extends Service {
  constructor(
    options: Partial<MemoryServiceOptions>,
    private app: Application
  ) {
    super(options);
  }

  async create(data: any, params: any) {
    console.log('detected create order', { data });
    notify(this.app);
    const _p = {
      query: {
        fields: ['*', 'units.*', 'cart.*', 'units.product.*'],
        filter: {},
      },
    };
    const order = await this.get(data.key, _p);
    return order;
  }

  async find(params: any): Promise<any> {
    const result = await Datastore()
      .items(COLLECTION_ORDER)
      .readByQuery(params.query || {});
    return result.data;
  }

  async update(id: number, data: any, params?: any): Promise<any> {
    const result = await Datastore()
      .items(COLLECTION_ORDER)
      .updateOne(id, data);
    return result;
  }

  async get(id: number, params?: any) {
    const result = await Datastore()
      .items(COLLECTION_ORDER)
      .readOne(id, params.query);
    return result;
  }
}

async function notify(app: Application) {
  const [admins, setting] = await Promise.all([
    app.service('admins').find({}),
    app.service('settings').find({}),
  ]);
  console.log({ admins, setting });
  const ids = admins.map((admin: any) => admin.adminDid);
  const storeName = setting.storeName || '';
  const did = app?.get('natadecoco_id');
  ids.forEach((recipient: string) => {
    app
      .service('messages')
      .create(
        {
          recipient,
          title: storeName,
          message:
            '新規の注文を受注しました。タップして注文情報を確認してください',
          did,
        },
        {}
      )
      .catch((err: any) => console.log(err.response.data.message));
    return;
  });
  return;
}
