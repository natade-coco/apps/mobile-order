import { Params } from "@feathersjs/feathers";
import { Service, MemoryServiceOptions } from "feathers-memory";
import { Application } from "../../declarations";
import { SDK as Hub } from "@natade-coco/hub-sdk";

export class Messages extends Service {
  constructor(
    options: Partial<MemoryServiceOptions>,
    private app: Application
  ) {
    super(options);
  }

  async create(data: any, params: Params) {
    const id = this.app.get("natadecoco_id");
    const secret = this.app.get("natadecoco_secret");
    const client = await Hub.init({
      id: id,
      secret: secret,
      test: this.app.get("dev_env") === "stg",
    });
    const result = await client.Notify({
      recipient: data.recipient,
      alert: data.title,
      message: data.message,
      url: data.url,
      did: data.did,
    });
    return super.create(result, params);
  }
}
