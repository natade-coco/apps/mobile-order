import { Application } from '../declarations';
import messages from './messages/messages.service';
import orders from './orders/orders.service';
import settings from './settings/settings.service';
import admins from './admins/admins.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(messages);
  app.configure(orders);
  app.configure(settings);
  app.configure(admins);
}
