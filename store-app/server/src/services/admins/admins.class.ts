import { Service, MemoryServiceOptions } from 'feathers-memory';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';

const COLLECTION_ADMIN = 'admins';
export interface Admin {
  id: number;
  date_created: string;
  date_updated: string;
  admin_did: string;
}

export class Admins extends Service {
  constructor(
    options: Partial<MemoryServiceOptions>,
    private app: Application
  ) {
    super(options);
  }

  async find(params?: any): Promise<any> {
    const result = await Datastore()
      .items(COLLECTION_ADMIN)
      .readByQuery(params.query || {});
    return result.data;
  }

  async create(data: any, params: any) {
    const result = await Datastore().items(COLLECTION_ADMIN).createOne(data);
    return result;
  }
}
