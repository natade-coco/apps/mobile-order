import { Service, MemoryServiceOptions } from "feathers-memory";
import { Application } from "../../declarations";
import { Datastore } from "../../datastore";

const COLLECTION_SETTING = "order_settings";
export class Settings extends Service {
  constructor(
    options: Partial<MemoryServiceOptions>,
    private app: Application
  ) {
    super(options);
  }

  async create(data: any, params: any) {
    return data;
  }

  async find(params: any): Promise<any> {
    const result = await Datastore().singleton(COLLECTION_SETTING).read();
    return result;
  }
}
