import { Directus, TypeMap } from '@directus/sdk';

import settings from './settings.json';

const collection = { name: 'settings', data: settings };

export async function seed(client: Directus<TypeMap>) {
  const result = await client.transport.patch(collection.name, collection.data);
  console.log(collection.name, 'items created');
}

export async function backup(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');

  const check = await client.transport.get(collection.name).catch(console.log);
  console.log('check');
  if (check && check.data) {
    console.log(collection.name + ' exists');
    fs.writeFileSync(
      path + '/' + collection.name + '.json',
      JSON.stringify(
        { storage_asset_presets: check.data.storage_asset_presets },
        null,
        '    '
      )
    );
    console.log(collection.name, 'items backup');
  }
}
