import { Directus, TypeMap } from '@directus/sdk';

import presets from './presets.json';

const collection = { name: 'presets', data: presets };

export async function seed(client: Directus<TypeMap>) {
  const result = await client.presets.createMany(collection.data);
  console.log(collection.name, 'items created');
}

export async function backup(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');

  const check = await client.presets.readByQuery().catch(console.log);
  if (check && check.data) {
    console.log(collection.name + ' exists');
    const filtered = check.data.map((item: any) => {
      delete item.user;
      delete item.id;
      return item;
    });
    fs.writeFileSync(
      path + '/' + collection.name + '.json',
      JSON.stringify(filtered, null, '    ')
    );
    console.log('items backup');
  }
}
