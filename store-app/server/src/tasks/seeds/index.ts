import { Directus, TypeMap } from '@directus/sdk';

import * as Settings from '../seeds/01_settings';
import * as Webhooks from '../seeds/02_webhooks';
import * as Presets from '../seeds/03_presets';
import * as Permissions from '../seeds/04_permissions';

export function seed(client: Directus<TypeMap>, internalURL: string) {
  Settings.seed(client);
  Webhooks.seed(client, internalURL);
  Presets.seed(client);
  Permissions.seed(client);
}

export function backup(client: Directus<TypeMap>) {
  Settings.backup(client, './src/tasks/seeds/01_settings');
  Webhooks.backup(client, './src/tasks/seeds/02_webhooks');
  Presets.backup(client, './src/tasks/seeds/03_presets');
  Permissions.backup(client, './src/tasks/seeds/04_permissions');
}
