import { Directus, TypeMap } from '@directus/sdk';

import * as Settings from '../migrations/01_settings';
import * as Catalogs from '../migrations/02_catalogs';
import * as Products from '../migrations/03_products';
import * as Prices from '../migrations/04_prices';
import * as ProductsCatalogs from '../migrations/05_products_catalogs';
import * as ProductsPrices from '../migrations/06_products_prices';
import * as Carts from '../migrations/07_carts';
import * as Orders from '../migrations/08_orders';
import * as OrderUnits from './09_order_units';
import * as Admins from './10_admins';

export async function setup(client: Directus<TypeMap>) {
  Settings.setup(client);
  Admins.setup(client);

  Promise.all([
    Products.setup(client),
    Catalogs.setup(client),
    Prices.setup(client),
    Carts.setup(client),
  ]).then(() => {
    ProductsCatalogs.setup(client);
    ProductsPrices.setup(client);
    Orders.setup(client).then(() => {
      OrderUnits.setup(client);
    });
  });
}

export async function teardown(client: Directus<TypeMap>) {
  Settings.teardown(client);
  Catalogs.teardown(client);
  Products.teardown(client);
  Prices.teardown(client);
  ProductsCatalogs.teardown(client);
  ProductsPrices.teardown(client);
  Carts.teardown(client);
  Orders.teardown(client);
  OrderUnits.teardown(client);
  Admins.teardown(client);
}

export async function dump(client: Directus<TypeMap>) {
  Settings.dump(client, './src/tasks/migrations/01_settings');
  Catalogs.dump(client, './src/tasks/migrations/02_catalogs');
  Products.dump(client, './src/tasks/migrations/03_products');
  Prices.dump(client, './src/tasks/migrations/04_prices');
  ProductsCatalogs.dump(client, './src/tasks/migrations/05_products_catalogs');
  ProductsPrices.dump(client, './src/tasks/migrations/06_products_prices');
  Carts.dump(client, './src/tasks/migrations/07_carts');
  Orders.dump(client, './src/tasks/migrations/08_orders');
  OrderUnits.dump(client, './src/tasks/migrations/09_order_units');
  Admins.dump(client, './src/tasks/migrations/10_admins');
}
