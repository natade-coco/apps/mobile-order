import { Directus, TypeMap } from '@directus/sdk';

import order_units from './order_units.json';
import order_units_relations from './order_units_relations.json';

const collection = { name: 'order_units', data: order_units };
const relation = { name: 'order_units_relations', data: order_units_relations };

export async function setup(client: Directus<TypeMap>) {
  client.collections
    .readOne(collection.name)
    .then(() => {
      console.log(collection.name, 'collection exists');
    })
    .catch(async () => {
      const result = await client.collections
        .createOne(collection.data)
        .catch(console.log);
      console.log(collection.name, 'collection created');
      await Promise.all(
        relation.data.map((item) => {
          return client.relations.createOne(item).catch(console.log);
        })
      );
      console.log(collection.name, 'relation created');
    });
}

export async function teardown(client: Directus<TypeMap>) {
  const check = await client.collections
    .readOne(collection.name)
    .catch(console.log);
  if (check) {
    const result = await client.collections
      .deleteOne(collection.name)
      .catch(console.log);
    console.log(collection.name, 'collection deleted');
  }
}

export async function dump(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');

  const [cData, fData] = await Promise.all([
    client.collections.readOne(collection.name).catch(console.log),
    client.fields.readMany(collection.name),
  ]);
  if (cData && fData) {
    let fields = (fData as any).map((item: any) => {
      delete item.meta.id;
      return item;
    });
    (cData as any).fields = fields;

    fs.writeFileSync(
      path + '/' + collection.name + '.json',
      JSON.stringify(cData, null, '    ')
    );
    console.log(collection.name, 'collection dump');
  }
  let rData: any = await client.relations.readMany(collection.name);
  if (rData) {
    rData = (rData as Array<any>).map((item) => {
      delete item.meta.id;
      return item;
    });

    fs.writeFileSync(
      path + '/' + relation.name + '.json',
      JSON.stringify(rData, null, '    ')
    );
    console.log(relation.name, 'relation dump');
  }
}
