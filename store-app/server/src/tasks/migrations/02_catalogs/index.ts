import { Directus, TypeMap } from '@directus/sdk';

import catalogs from './catalogs.json';

const collection = { name: 'catalogs', data: catalogs };

export async function setup(client: Directus<TypeMap>) {
  client.collections
    .readOne(collection.name)
    .then(() => {
      console.log(collection.name, 'collection exists');
    })
    .catch(async () => {
      await client.collections.createOne(collection.data).catch(console.log);
      console.log(collection.name, 'collection created');
    });
}

export async function teardown(client: Directus<TypeMap>) {
  const check = await client.collections
    .readOne(collection.name)
    .catch(console.log);
  if (check) {
    const result = await client.collections
      .deleteOne(collection.name)
      .catch(console.log);
    console.log(collection.name, 'collection deleted');
  }
}

export async function dump(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');

  const [cData, fData] = await Promise.all([
    client.collections.readOne(collection.name),
    client.fields.readMany(collection.name),
  ]);
  if (cData && fData) {
    let fields = (fData as any).map((item: any) => {
      delete item.meta.id;
      return item;
    });
    (cData as any).fields = fields;

    fs.writeFileSync(
      path + '/' + collection.name + '.json',
      JSON.stringify(cData, null, '    ')
    );
    console.log(collection.name, 'collection dump');
  }
}
