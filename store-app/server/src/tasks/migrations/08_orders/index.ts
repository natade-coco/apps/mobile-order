import { Directus, TypeMap } from '@directus/sdk';

import orders from './orders.json';
import orders_relations from './orders_relations.json';

const collection = { name: 'orders', data: orders };
const relation = { name: 'orders_relations', data: orders_relations };

export async function setup(client: Directus<TypeMap>) {
  client.collections
    .readOne(collection.name)
    .then(() => {
      console.log(collection.name, 'collection exists');
    })
    .catch(async () => {
      await client.collections.createOne(collection.data).catch(console.log);
      console.log(collection.name, 'collection created');
    });
}

export async function teardown(client: Directus<TypeMap>) {
  const check = await client.collections
    .readOne(collection.name)
    .catch(console.log);
  if (check) {
    const result = await client.collections
      .deleteOne(collection.name)
      .catch(console.log);
    console.log(collection.name, 'collection deleted');
  }
}

export async function dump(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');

  const [cData, fData] = await Promise.all([
    client.collections.readOne(collection.name).catch(console.log),
    client.fields.readMany(collection.name),
  ]);
  if (cData && fData) {
    let fields = (fData as any).map((item: any) => {
      delete item.meta.id;
      return item;
    });
    (cData as any).fields = fields;

    fs.writeFileSync(
      path + '/' + collection.name + '.json',
      JSON.stringify(cData, null, '    ')
    );
    console.log(collection.name, 'collection dump');
  }
  let rData: any = await client.relations.readMany(collection.name);
  if (rData) {
    rData = (rData as Array<any>).map((item) => {
      delete item.meta.id;
      return item;
    });

    fs.writeFileSync(
      path + '/' + relation.name + '.json',
      JSON.stringify(rData, null, '    ')
    );
    console.log(relation.name, 'relation dump');
  }
}
