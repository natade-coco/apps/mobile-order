const activeEnv = process.env.GATSBY_ACTIVE_ENV || process.env.NODE_ENV || "development"
console.log(`Using environment config: '${activeEnv}'`)
require("dotenv").config({
  path: `.env.${activeEnv}`
})

module.exports = {
  siteMetadata: {
    title: `Mobile Order store`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-scss-typescript`,
    {
      resolve: "@sentry/gatsby",
      options: {
        dsn: "https://d60528e79b9343fbae305d2b21c81114@o416470.ingest.sentry.io/5669758"
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    }
  ]
}
