import { combineReducers } from '@reduxjs/toolkit'
import AppReducer from '../slices/app'
import OrderReducer from '../slices/order'
import SettingReducer from '../slices/setting'
import {
  TypedUseSelectorHook,
  useSelector as rawUseSelector
} from 'react-redux'

const rootReducer = combineReducers({
  app: AppReducer,
  order: OrderReducer,
  setting: SettingReducer
})

export type RootState = ReturnType<typeof rootReducer>

export const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector

export default rootReducer
