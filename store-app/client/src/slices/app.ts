import {
  createSlice,
  PayloadAction,
  createSelector,
  createAsyncThunk,
  isPending,
  isRejected,
  isFulfilled,
  createAction
} from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Id, Session } from '../entities/app'
import { Order } from '../entities/order'
import PocketService from '../services/pocket'
import AdminService from '../services/server/admin'
import AppService from '../services/server/app'
import OrderService from '../services/server/order'

const appService = new AppService()
const orderService = new OrderService()
const adminService = new AdminService()
const pocket = new PocketService()

export interface State {
  session: Session
  pendingActions: string[]
  error: string | null
  pendingOrders: Id[]
  showError: boolean
}

const initialState: State = {
  pendingActions: [],
  error: null,
  session: null,
  pendingOrders: [],
  showError: false
}

// Selectors

const appISelector = (state: RootState) => state.app
export const isLoadingSelector = createSelector(appISelector, (state) => state.pendingActions.length > 0)
export const sessionSelector = createSelector(appISelector, (state) => state.session)
export const pendingOrdersSelector = createSelector(appISelector, (state) => state.pendingOrders)
export const errorSelector = createSelector(appISelector, (state) => state.error)
export const showErrorSelector = createSelector(appISelector, (state) => state.showError)

// Action

export const appStart = createAsyncThunk('app/start', async (cb: (order: Order) => void): Promise<Session> => {
  try {
    orderService.setSubscribeCallback(cb)
    const jwt = await pocket.requestSignJWT()
    const session = await appService.getSession(jwt)
    adminService.setAdmin(session.user.id)
    return session
  } catch (err) {
    throw err
  }
})
export const setShowError = createAction<boolean>('app/setShowError')
export const addPendingOrder = createAction<Id>('app/addPendingOrder')
export const deletePendingOrder = createAction<Id>('app/deletePendingOrder')
// Slice

const getOriginalActionType = (action: PayloadAction<any>): string => action.type.slice(0, action.type.lastIndexOf('/'))

const startLoading = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.add(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
}

const loadingSuccess = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
  state.error = null
}

const loadingFailed = (state: State, action: PayloadAction<any, string, any, any>) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
  state.error = action.error?.message
}
const slice = createSlice({
  name: 'app',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(appStart.fulfilled, (state, { payload }) => {
        state.session = payload
      })
      .addCase(setShowError, (state, { payload }) => {
        state.showError = payload
      })
      .addCase(addPendingOrder, (state, { payload }) => {
        const set = new Set(state.pendingOrders)
        set.add(payload)
        state.pendingOrders = Array.from(set)
      })
      .addCase(deletePendingOrder, (state, { payload }) => {
        const set = new Set(state.pendingOrders)
        set.delete(payload)
        state.pendingOrders = Array.from(set)
      })
      .addMatcher(isPending, startLoading)
      .addMatcher(isFulfilled, loadingSuccess)
      .addMatcher(isRejected, loadingFailed)
  }
})

export default slice.reducer
