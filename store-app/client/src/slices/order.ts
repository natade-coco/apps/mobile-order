import { createAction, createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Id } from '../entities/app'
import { Order, OrderParams } from '../entities/order'
import OrderService from '../services/server/order'

const orderService = new OrderService()

const entityAdapter = createEntityAdapter<Order>()

const orderISelector = (state: RootState) => state.order

export const orderSelectors = entityAdapter.getSelectors(orderISelector)

export const getOrders = createAsyncThunk(
  'order/getList',
  async (payload?: { startTime?: string; finishTime?: string }): Promise<Order[]> => {
    try {
      const orders = await orderService.getOrders(payload?.startTime, payload?.finishTime)
      return orders
    } catch (err) {
      throw err
    }
  }
)

export const updateOrder = createAsyncThunk(
  'order/update',
  async (payload: { id: Id; data: Partial<OrderParams> }): Promise<Order> => {
    try {
      const { id, data } = payload
      const order = await orderService.updateOrder(id, data)
      return order
    } catch (err) {
      throw err
    }
  }
)

export const addOrder = createAction<Order>('order/add')

const slice = createSlice({
  name: 'order',
  initialState: entityAdapter.getInitialState(),
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getOrders.fulfilled, entityAdapter.setAll)
      .addCase(updateOrder.fulfilled, (state, { payload }) => {
        const entity = state.entities[payload.id]
        entity.state = payload.state
      })
      .addCase(addOrder, entityAdapter.upsertOne)
  }
})

export default slice.reducer
