import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { RouteComponentProps } from '@reach/router'

import { useSelector } from '../app/rootReducer'
import { AppDispatch } from '../app/store'
import { OrderState } from '../entities/order'
import { getOrders, orderSelectors, updateOrder } from '../slices/order'
import { pendingOrdersSelector, sessionSelector } from '../slices/app'
import OrderList from '../components/OrderList'
import OrderMenu from '../components/OrderMenu'

interface Props extends RouteComponentProps {}

const Feature = (props: Props) => {
  useEffect(() => {
    dispatch(getOrders({ startTime: setting.receptionStartTime, finishTime: setting.receptionFinishTime }))
  }, [])

  const [activeMenu, setActiveMenu] = useState<OrderState>('accepted')
  const dispatch: AppDispatch = useDispatch()

  const changeTab = (menu: OrderState) => setActiveMenu(menu)
  const onCancel = (id: number) => {
    dispatch(updateOrder({ id, data: { state: 'canceled' } }))
  }
  const onChangeState = (id: number, state: OrderState) => {
    const updateState = state === 'accepted' ? 'prepared' : 'served'
    dispatch(updateOrder({ id, data: { state: updateState } }))
  }

  const orders = useSelector(orderSelectors.selectAll)
  const setting = useSelector((state) => state.setting)
  const session = useSelector(sessionSelector)
  const pendingOrders = useSelector(pendingOrdersSelector)

  const orderMenuProps = { activeMenu, orders, changeTab }
  const orderListPorps = {
    activeMenu,
    orders,
    pendingOrders,
    env: session.env,
    taxRate: setting.taxRate,
    isPrePay: setting.isPrepay,
    onCancel,
    onChangeState
  }

  return (
    <div className="section">
      <OrderHeader />
      <OrderMenu {...orderMenuProps} />
      <OrderList {...orderListPorps} />
    </div>
  )
}

export default Feature

const OrderHeader = () => {
  return (
    <div className="columns is-mobile">
      <div className="column is-half is-offset-one-quarter">
        <h5 className="is-4 subtitle has-text-centered has-text-weight-bold">注文リスト</h5>
      </div>
    </div>
  )
}
