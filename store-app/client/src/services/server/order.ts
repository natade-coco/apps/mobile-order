import { Application, FeathersService, Id, Service } from '@feathersjs/feathers/lib'
import { serverApp } from './index'
import { OrderParams, Order } from '../../entities/order'
import moment from 'moment'

export default class OrderService {
  service: FeathersService<Application, Service<Order>>
  constructor() {
    this.service = serverApp?.service('orders')
  }
  getOrders = async (startTime?: string, stopTime?: string): Promise<Order[]> => {
    const params = {
      query: {
        fields: ['*', 'units.*', 'cart.*', 'units.product.*'],
        filter: {}
      }
    }
    const startDate = '$NOW(-1 day)'
    const stopDate = '$NOW'

    params.query.filter = {
      date_created: {
        _between: `${startDate},${stopDate}`
      }
    }
    const result = await this.service.find(params)
    if (result instanceof Array) {
      return result
    }
    return [result]
  }

  updateOrder = async (id: Id, data: Partial<OrderParams>): Promise<Order> => {
    const result = await this.service.update(id, data)
    if (result instanceof Array) {
      return result[0]
    }
    return result
  }

  setSubscribeCallback = (cb: (order: Order) => void) => {
    this.service.on('created', cb)
  }
}
