import { Application, FeathersService, Params, Service } from '@feathersjs/feathers/lib'
import { serverApp } from './index'
import { Admin } from '../../entities/admin'

export default class AdminService {
  private service: FeathersService<Application, Service<Admin>>
  constructor() {
    this.service = serverApp?.service('admins')
  }

  findAdmin = async (query: Params) => {
    const admins = await this.service.find({ query })
    if (admins instanceof Array) {
      return admins[0]
    }
    return admins
  }

  setAdmin = async (did: string) => {
    const query = {
      filter: {
        admin_did: {
          _eq: did
        }
      }
    }
    const admin = await this.findAdmin(query)
    console.log({ admin })
    if (!admin) {
      const result = await this.service.create({ adminDid: did })
      return result
    }
    return null
  }
}
