import moment from 'moment'
export const parseDate = (date: string) => {
  return moment(date).utc(true).local().format('MM/DD HH:mm')
}

export const getDiffTime = (date: string) => {
  const nowMoment = moment()
  const target = moment(date).utc(true).local()
  let diff: any = nowMoment.diff(target, 'day')
  let text = '日前'
  if (diff === 0) {
    diff = nowMoment.diff(target, 'hour')
    text = '時間前'
    if (diff === 0) {
      diff = nowMoment.diff(target, 'minute')
      text = '分前'
      if (diff === 0) {
        diff = nowMoment.diff(target, 'second')
        text = '秒前'
      }
    }
  }
  return `${diff} ${text}`
}
