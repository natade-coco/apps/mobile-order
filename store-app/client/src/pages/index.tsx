import React, { useEffect, useState } from 'react'

import Layout from '../components/Layout'
import Router from '../components/Router'

import StoreOrder from '../features/Order'
import HashLoader from 'react-spinners/HashLoader'

import detectedSound from '../assets/detectedSound.mp3'
import { useDispatch, useSelector } from 'react-redux'
import ErrorModal from '../components/Error'

import './index.scss'
import { appStart, errorSelector, showErrorSelector } from '../slices/app'
import { addOrder } from '../slices/order'
import { AppDispatch } from '../app/store'
import { Order } from '../entities/order'
import { getSetting } from '../slices/setting'

const Page = () => {
  const dispatch: AppDispatch = useDispatch()
  const [isAppReady, setIsAppReady] = useState(false)

  const error = useSelector(errorSelector)
  const showError = useSelector(showErrorSelector)

  useEffect(() => {
    const sound = new Audio(detectedSound)
    const cb = (order: Order) => {
      dispatch(addOrder(order))
      sound.play().catch((e) => {
        console.log(e)
      })
      return order
    }
    dispatch(appStart(cb)).then(() => {
      dispatch(getSetting())
      setIsAppReady(true)
    })
  }, [])

  const onCloseErrorModal = () => {
    window.location.reload()
  }

  return (
    <>
      {isAppReady ? (
        <Layout>
          <Router>
            <StoreOrder path="/" />
          </Router>
          <ErrorModal message={error} title={'Error'} onClose={onCloseErrorModal} showNotice={showError} />
        </Layout>
      ) : (
        <Loader />
      )}
    </>
  )
}

const Loader = () => (
  <div className="hero is-fullheight">
    <div className="hero-body">
      <div className="container" style={{ flex: '0 0 auto' }}>
        <HashLoader size={100} color={'#09b4c6'} loading />
      </div>
    </div>
  </div>
)

export default Page
