export type OrderState = 'accepted' | 'prepared' | 'served' | 'canceled'

export type PaymentType = 'Serve' | 'Pull'

export type PublishStatus = 'published' | 'draft'

export type ServeState = 'in_service' | 'stopped'

export type Cart = {
  id: number
  dateCreated: string
  dateUpdated: string
  userId: string
  orders?: Order[]
}

export type Order = {
  id: number
  dateCreated: string
  dateUpdated: string
  reference: number
  state: OrderState
  cart: Cart
  email?: string
  units?: OrderUnit[]
  isRead: boolean
  txId?: string
  chargeId?: string
  receiptUrl?: string
}

export type OrderParams = Omit<Order, 'id' | 'dateCreated' | 'dateUpdated'>

export type OrderUnit = {
  id: number
  dateCreated: string
  dateUpdated: string
  price: number
  title: string
  caption: string
  quantity: number
  order?: number
  product?: Product
}

export type Product = {
  id: number
  status: PublishStatus
  dateCreated: string
  dateUpdated: string
  catalogs: number[] | null
  title: string
  price: number
  subtitle?: string
  description?: string
  // estimated_time?: number;
  image?: number
  imageUrl?: string
  state?: ServeState
}
