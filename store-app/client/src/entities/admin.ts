export interface Admin {
  id: number
  dateCreated: string
  dateUpdated: string
  adminDid: string
}
