export interface Setting {
  dateCreated: string
  dateUpdated: string
  taxRate: number
  receptionStartTime: string
  receptionFinishTime: string
  storeName: string
  isIncludedTax: boolean
  isPrepay: boolean
}
