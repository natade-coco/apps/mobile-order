declare const graphql: (query: TemplateStringsArray) => void
declare const wasm: any
declare module '*.svg' {
  const content: string
  export default content
}
declare module '*.mp3'

declare module '*.png' {
  const content: string
  export default content
}
