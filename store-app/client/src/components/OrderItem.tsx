import * as React from 'react'
import { useEffect, useState } from 'react'
import { Order, OrderState } from '../entities/order'
import { getDiffTime } from '../util'
import OrderDetail from './OrderDetail'
import StateBtn from './StateBtn'

type OrderItemProps = {
  order: Order
  env: string
  isPrepay: boolean
  taxRate: number
  isLoadingStateBtn: boolean
  onCancel: (id: number) => void
  onClickStateBtn: (id: number, state: OrderState) => void
}

const OrderItem = (props: OrderItemProps) => {
  const { order, env, onCancel, taxRate, isLoadingStateBtn, onClickStateBtn, isPrepay } = props
  const sumPrice = order.units.reduce((acc, item) => {
    return Math.round(item.price * item.quantity + acc)
  }, 0)
  const sumTax = order.units.reduce((acc, item) => {
    return item.quantity * Math.floor((item.price / (1 + taxRate * 0.01)) * (taxRate / 100)) + acc
  }, 0)

  const [time, setTime] = useState('')
  useEffect(() => {
    setTime(getDiffTime(order.dateCreated))
    const time = setInterval(() => {
      setTime(getDiffTime(order.dateCreated))
    }, 1000 * 10)
    return () => {
      clearInterval(time)
    }
  }, [order])

  return (
    <div className="box order py-3" style={{ border: '1px solid #bbb' }}>
      <div className="is-size-5">
        <div className="is-size-5 is-flex is-flex-direction-row is-align-items-center is-justify-content-space-between">
          <span>
            注文番号
            <span className={`has-text-weight-bold is-size-3`}> {order.reference}</span>
          </span>
          {isPrepay ||
            (!isPrepay && order.state !== 'canceled' && (
              <OrderDetail order={order} onCancel={onCancel} env={env} isPrepay={isPrepay} />
            ))}
        </div>
        <div className="is-flex is-flex-direction-row is-align-items-center is-justify-content-space-between">
          <p>
            <i className="far fa-clock has-text-grey" aria-hidden="true"></i> {time}
          </p>
          {order.state === 'canceled' && (
            <span className="is-light tag is-danger is-medium has-text-weight-bold ml-3">キャンセル済</span>
          )}
        </div>
        <div className="my-3">
          {order.units.map((item, index) => {
            return (
              <li
                className=""
                style={{
                  display: 'table',
                  width: '100%',
                  borderBottom: '1px solid #d6d5d5'
                }}
                key={index}
              >
                <div
                  className="is-size-4 has-text-weight-bold"
                  style={{ display: 'table-cell', verticalAlign: 'middle' }}
                >
                  {item.title}
                  {item.caption && `(${item.caption})`}
                </div>
                <div
                  className="has-text-right is-size-6"
                  style={{
                    display: 'table-cell',
                    minWidth: '5rem',
                    verticalAlign: 'middle'
                  }}
                >
                  <span>
                    × <span className="is-size-4 has-text-weight-bold">{item.quantity}</span>点
                  </span>
                </div>
                <hr className="divider" />
              </li>
            )
          })}
        </div>

        <div className="mt-2" style={{ display: 'table', width: '100%' }}>
          <div className="" style={{ display: 'table-cell' }}>
            合計:
          </div>
          <div className="has-text-right" style={{ display: 'table-cell' }}>
            ¥
            <span className="is-size-3 has-text-weight-bold has-text-danger">
              {Math.round(sumPrice).toLocaleString()}
            </span>
          </div>
        </div>
        <div style={{ display: 'table', width: '100%' }}>
          <div className="" style={{ display: 'table-cell' }}>
            (うち消費税
          </div>
          <div className="has-text-right" style={{ display: 'table-cell' }}>
            ¥{Math.round(sumTax).toLocaleString()})
          </div>
        </div>
        <StateBtn isLoading={isLoadingStateBtn} onChangeState={onClickStateBtn} order={order} />
      </div>
    </div>
  )
}

export default OrderItem
