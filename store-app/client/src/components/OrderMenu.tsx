import React from 'react'
import { Order, OrderState } from '../entities/order'

type OrderMenuProps = {
  activeMenu: OrderState
  changeTab: (menu: OrderState) => void
  orders: Order[]
}

const OrderMenu = (props: OrderMenuProps) => {
  const { activeMenu, changeTab, orders } = props
  const tabMenuData: { name: string; state: OrderState; className: string }[] = [
    {
      name: '受注',
      state: 'accepted',
      className: 'badge has-background-primary'
    },
    {
      name: '受渡',
      state: 'prepared',
      className: 'badge has-background-info'
    },
    { name: '完了', state: 'served', className: 'badge has-background-grey' }
  ]

  return (
    <div>
      <div className="tabs is-boxed is-medium" style={{ overflow: 'visible' }}>
        <ul>
          {tabMenuData.map((content, index) => (
            <li
              key={index}
              className={`is-relative	 ${activeMenu === content.state ? 'is-active has-text-weight-bold' : ''}`}
              onClick={() => changeTab(content.state)}
            >
              <a>
                <span className="px-3 has-text-black">{content.name}</span>
              </a>
              <div className={content.className}>
                <span>
                  {
                    orders.filter(
                      (order) =>
                        order.state === content.state || (order.state === 'canceled' && content.state === 'served')
                    ).length
                  }
                </span>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default OrderMenu
