import * as React from 'react'
import { Order, OrderState } from '../entities/order'

type StateBtnProps = {
  order: Order
  onChangeState: (id: number, state: OrderState) => void
  isLoading: boolean
}

const StateBtn = ({ order, isLoading, onChangeState }: StateBtnProps) => {
  type StateDict<T extends keyof any> = { [key in T]: { text: string; className: string } }
  const stateDict: StateDict<OrderState> = {
    accepted: {
      text: '準備完了',
      className: 'is-primary has-text-black'
    },
    prepared: {
      text: '受渡完了',
      className: 'has-background-grey has-text-white'
    },
    served: { text: '', className: 'is-hidden' },
    canceled: { text: '', className: 'is-hidden' }
  }
  return (
    <button
      className={`button column is-info is-fullwidth is-rounded is-medium ${stateDict[order.state].className} mt-4 ${
        isLoading ? 'is-loading' : ''
      }`}
      onClick={() => {
        onChangeState(order.id, order.state)
      }}
    >
      <span className="is-size-5">{stateDict[order.state].text}</span>
      <span className="icon is-small">
        <i className="fas fa-arrow-right"></i>
      </span>
    </button>
  )
}

export default StateBtn
