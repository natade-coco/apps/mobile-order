import * as React from 'react'
import { useState, useEffect } from 'react'
import externalLink from '../images/external-link.svg'
import externalLinkRed from '../images/external-link_red.svg'
import Pocket from '@natade-coco/pocket-sdk'
import { Order } from '../entities/order'

const STRIPE_PAYMENT_URL = (ip: string, isStg?: boolean) =>
  `https://dashboard.stripe.com/${isStg ? 'test/' : ''}payments/${ip}`

type OrderDetailProps = {
  order: Order
  onCancel: (id: number) => void
  env?: string
  isPrepay: boolean
}

const OrderDetail = (props: OrderDetailProps) => {
  const { order, onCancel, env, isPrepay } = props
  const [open, setOpen] = useState(false)
  const detail = document.getElementById(`doropdown-detail-${props.order.id}`)
  useEffect(() => {
    setOpen(false)
  }, [props.order])

  const onRefund = () => {
    const url = STRIPE_PAYMENT_URL(order.chargeId, env === 'stg')
    Pocket.openInBrowser(url).catch((error) => {
      if (error === 'incompatible') {
        window.open(url, '_blank')
      }
    })
  }
  const onOpenReceipt = () => {
    const url = props.order.receiptUrl
    Pocket.openInBrowser(url).catch((error) => {
      if (error === 'incompatible') {
        window.open(url, '_blank')
      }
    })
  }

  return (
    <div className={`dropdown ${open ? 'is-active' : ''} is-right`} id={`doropdown-detail-${props.order.id}`}>
      <div className="dropdown-trigger">
        <button
          className="button"
          aria-haspopup="true"
          aria-controls="dropdown-menu"
          style={{ border: 'none' }}
          onClick={() => {
            setOpen(!open)
          }}
        >
          <span className="icon is-small">
            <i className="fas fa-ellipsis-h" aria-hidden="true"></i>
          </span>
        </button>
      </div>
      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
          {isPrepay ? (
            <>
              <span className="dropdown-item">
                <ReceiptBtn onOpen={onOpenReceipt} />
              </span>
              <span
                className="dropdown-item mt-4 m-3 py-4 px-2"
                style={{ border: '1px solid #f50042', borderRadius: '0.5rem' }}
              >
                <RefundBtn onRefund={onRefund} />
                {!['canceled'].includes(props.order.state) && <CancelBtn onCancel={() => onCancel(props.order.id)} />}
              </span>
            </>
          ) : (
            !['canceled'].includes(props.order.state) && (
              <span className="dropdown-item">
                <CancelBtn onCancel={() => onCancel(props.order.id)} />
              </span>
            )
          )}
        </div>
      </div>
    </div>
  )
}

export default OrderDetail

type CancelBtnProps = {
  onCancel: () => void
}

const CancelBtn = (props: CancelBtnProps) => {
  const { onCancel } = props
  return (
    <button className="button is-light is-danger has-text-weight-bold" onClick={() => onCancel()}>
      注文をキャンセル
    </button>
  )
}

type RefundBtnProps = {
  onRefund: () => void
}

const RefundBtn = (props: RefundBtnProps) => {
  const { onRefund } = props
  return (
    <button
      onClick={() => onRefund()}
      style={{ width: '100%' }}
      className="button is-light is-danger has-text-weight-bold mb-3"
    >
      返金手続きへ
      <img className="" src={externalLinkRed} style={{ height: '1rem', marginLeft: '0.2rem' }} />
    </button>
  )
}

type ReceiptBtnProps = {
  onOpen: () => void
}
const ReceiptBtn = (props: ReceiptBtnProps) => {
  const { onOpen } = props
  return (
    <button
      onClick={() => onOpen()}
      style={{ width: '100%' }}
      className="button is-light is-white has-text-weight-bold"
    >
      電子領収書
      <img src={externalLink} style={{ height: '1rem', marginLeft: '0.2rem' }} />
    </button>
  )
}
