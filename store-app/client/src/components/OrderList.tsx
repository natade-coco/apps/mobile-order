import * as React from 'react'
import { Order, OrderState } from '../entities/order'
import { Id } from '../entities/app'
import OrderItem from './OrderItem'

type OrderListProps = {
  orders: Order[]
  activeMenu: OrderState
  pendingOrders: Id[]
  env: string
  taxRate: number
  isPrePay: boolean
  onCancel: (id: number) => void
  onChangeState: (id: number, state: OrderState) => void
}

const OrderList = (props: OrderListProps) => {
  const { orders, activeMenu, pendingOrders, env, taxRate, isPrePay, onCancel, onChangeState } = props
  const target = orders.filter(
    (order) => order.state === activeMenu || (order.state === 'canceled' && activeMenu === 'served')
  )
  return (
    <div
      className="box column"
      style={{
        borderWidth: `0px 1px 1px`,
        borderStyle: `solid`,
        borderColor: `#dbdbdb`,
        borderRadius: `0px`,
        maxHeight: '80vh',
        overflow: 'scroll'
      }}
    >
      {activeMenu === 'accepted' && target.length > 0 && (
        <article className="message is-size-5">
          <div className="message-body">
            <span>
              準備完了ボタンをタップすると
              <span className="tag is-warning is-light is-size-6">受取可</span>
              として注文者にお知らせします
            </span>
          </div>
        </article>
      )}
      {target.length > 0 ? (
        target.map((order, index) => {
          return (
            <div key={index} className="mt-3">
              <OrderItem
                isLoadingStateBtn={!!pendingOrders.find((id) => id === order.id)}
                onClickStateBtn={onChangeState}
                order={order}
                env={env}
                onCancel={onCancel}
                taxRate={taxRate}
                isPrepay={isPrePay}
              />
            </div>
          )
        })
      ) : (
        <article className="message is-size-5">
          <div className="message-body">
            <span>該当する注文がありません</span>
          </div>
        </article>
      )}
    </div>
  )
}

export default OrderList
