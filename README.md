# Mobile Order
このリポジトリは、[natadeCOCO](https://natade-coco.com)でモバイルオーダーをお試しできるサンプルアプリとして公開しています。

## ディレクトリ構成

```
├── README.md
├── .gitlab-ci.yml
├── consumer-app(注文アプリ)
│   ├── Dockerfile
│   ├── client(クライアント側)
│   └── server(サーバー側)
└── store-app(受注アプリ)
    ├── Dockerfile
    ├── client(クライアント側)
    └── server(サーバー側)
```

## 活用事例
https://natade-coco.com/casestudy/scene/foodhall/

## 使用ガイド
https://www.natade-coco.com/guide/#/case-study/mobile-order
